﻿unit Unit1;


interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Math, UMorfer;

type

  { TForm1 }

  TForm1 = class(TForm)
    btnSearch: TButton;
    Edit1: TEdit;
    Memo1: TMemo;
    btnRebuildIndex: TButton;
    btnSaveIndex: TButton;
    GroupBox1: TGroupBox;
    procedure btnSearchClick(Sender: TObject);
    procedure btnRebuildIndexClick(Sender: TObject);
    procedure btnSaveIndexClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

type
  TArrayOfInt = array of integer;

  PLexemNode = ^TLexemNode;

  TLexemNode = record
    LexemId: integer; // ключ
    Parent:PLexemNode;
    Left, Right: PLexemNode;
  end;

  TLexem = record
    LexemId: integer;
    Lexem: WideString;
  end;

  TFilesOfWord = record
    GlobalWordId: integer;
    FileIdList: TArrayOfInt;
  end;

  TFilesOfLexem = array of TFilesOfWord;

  TWord = record
    LexemId: integer;
    Value: WideString;
    Filtered: Boolean;
    Pozes: TArrayOfInt;
  end;

  TWordMap = array of TWord;

  TFilesDescriptor = record
    IndexFileName: WideString;
    FileName: WideString;
    LengthBlob: integer;
    WordMap: TWordMap;
    Blob: TArrayOfInt; // Список слов.
  end;

var
  LexemTree: PLexemNode;
  Lexems: array of TLexem;
  Words: array of WideString;
  LexemIndex: array of TFilesOfLexem;
  FilesDescriptor: array of TFilesDescriptor;
  FilesName:array of WideString;
  IsIndexValidate:Boolean=False;

implementation

{$R *.dfm}

function LexemTreeSearch(Node:PLexemNode;const value:WideString): PLexemNode;
begin
 { result:=nil;
  if (Node=nil) or (Lexems[Node.LexemId].Lexem=value) then result:=Node
    else
    if Value<Lexems[Node.LexemId].Lexem then
       result:=LexemTreeSearch(Node.Left, value)
       else
       result:=LexemTreeSearch(Node.Right, value);}

  result:=nil;
  while (Node<>nil) and (Lexems[Node.LexemId].Lexem<>value) do
     begin
     if Value<Lexems[Node.LexemId].Lexem then
        Node:=Node.Left
        else
        Node:=Node.Right;
     end;
  result:=Node;
end;

procedure LexemTreeInsert(Node:PLexemNode;const addedNode:PLexemNode);
var ParentNode:PLexemNode;
begin
  ParentNode:=nil;
  while Node<>nil do
     begin
       ParentNode:=Node;
       if Lexems[addedNode.LexemId].Lexem<Lexems[Node.LexemId].Lexem then
          Node:=Node.Left
          else
          Node:=Node.Right;
     end;
  addedNode.Parent:=ParentNode;
  if ParentNode=nil then
       LexemTree:=addedNode // дерево пустое
      else
       if Lexems[addedNode.LexemId].Lexem<Lexems[ParentNode.LexemId].Lexem then
          ParentNode.Left:=addedNode
          else
          ParentNode.Right:=addedNode;

end;

// Удаляем дерево
procedure LexemTreeFree(Node:PLexemNode);
begin
  if (Node<>nil) then
    begin
    if Node.Left<>nil then LexemTreeFree(Node.Left);
    if Node.Right<>nil then LexemTreeFree(Node.Right);
    Dispose(Node);
    end;
end;

procedure WriteWideString(FS:TFileStream; value:WideString);
var
  Count:Integer;
  P:PWideChar;
begin
  Count:=Length(value);
  FS.Write(Count,SizeOf(Count));
  if Count>0 then
     begin
     P:=@value[1];
     FS.Write(P^,Count*SizeOf(WideChar));
     end;
end;

procedure WriteArraOfInt(FS:TFileStream; value:TArrayOfInt);
var
  Count:Integer;
  P:PWideChar;
begin
  Count:=Length(value);
  FS.Write(Count,SizeOf(Count));
  if Count>0 then
     begin
     P:=@value[0];
     FS.Write(P^,Count*SizeOf(Integer));
     end;
end;

procedure WriteLexems(FS:TFileStream);
var i:integer;
  Count:Integer;
begin
  Count:=Length(Lexems);
  FS.Write(Count,SizeOf(Count));
  for i:=0 to Length(Lexems)-1 do
    begin
    FS.Write(Lexems[i].LexemId,SizeOf(Integer));
    WriteWideString(FS,Lexems[i].Lexem);
    end;
end;

procedure WriteWords(FS:TFileStream);
var i:integer;
  Count:Integer;
begin
  Count:=Length(Words);
  FS.Write(Count,SizeOf(Count));
  for i:=0 to Length(Words)-1 do
    WriteWideString(FS,Words[i]);
end;

procedure WriteFilesOfWord(FS:TFileStream; value:TFilesOfWord);
var WiD:Integer;
begin
  WiD:=value.GlobalWordId;
  FS.Write(WiD,SizeOf(WiD));
  WriteArraOfInt(FS,value.FileIdList);
end;

procedure WriteFilesOfLexem(FS:TFileStream; value:TFilesOfLexem);
var i:integer;
  Count:Integer;
begin
  Count:=Length(value);
  FS.Write(Count,SizeOf(Count));
  for i:=0 to Length(value)-1 do
    begin
     WriteFilesOfWord(FS,value[i]);
    end;
end;

procedure WriteLexemIndex(FS:TFileStream);
var i:integer;
  Count:Integer;
begin
  Count:=Length(LexemIndex);
  FS.Write(Count,SizeOf(Count));
  for i:=0 to Length(LexemIndex)-1 do
    begin
     WriteFilesOfLexem(FS,LexemIndex[i]);
    end;
end;

procedure WriteFilesName(FS:TFileStream);
var i:integer;
  Count:Integer;
begin
  Count:=Length(FilesName);
  FS.Write(Count,SizeOf(Count));
  for i:=0 to Length(FilesName)-1 do
    WriteWideString(FS,FilesName[i]);
end;

procedure SaveIndex(FileName:String);
var FS:TFileStream;
begin
  FS:=TFileStream.Create(FileName,fmCreate or fmOpenWrite);
  WriteLexems(FS);
  WriteWords(FS);
  WriteLexemIndex(FS);
  WriteFilesName(FS);
  FS.Destroy;
end;

function ReadWideString(FS:TFileStream; var value:WideString):Boolean;
var
  Count:Integer;
  P:PWideChar;
begin
  if FS.Read(Count,SizeOf(Count))<SizeOf(Count) then exit(False);
  SetLength(Value,Count);
  if Count>0 then
     begin
     P:=@value[1];
     if FS.Read(P^,Count*SizeOf(WideChar))<Count*SizeOf(WideChar) then exit(False);
     end;
   exit(True);
end;

function ReadArraOfInt(FS:TFileStream;var value:TArrayOfInt):Boolean;
var
  Count:Integer;
  P:PByte;
begin
  if FS.Read(Count,SizeOf(Count))< SizeOf(Count) then Exit(False);
  SetLength(Value,Count);
  if Count>0 then
     begin
     P:=@value[0];
     if FS.Read(P^,Count*SizeOf(Integer))<Count*SizeOf(Integer) then exit(False);
     end;
  exit(True);
end;

function ReadLexems(FS:TFileStream):Boolean;
var i:integer;
  Count:Integer;
  LexemNode:PLexemNode;
begin
  if FS.Read(Count,SizeOf(Count))<SizeOf(Count) then exit(False);
  SetLength(Lexems,Count);
  for i:=0 to Length(Lexems)-1 do
    begin
    if FS.Read(Lexems[i].LexemId,SizeOf(Integer))<SizeOf(Integer) then
       exit(False);
    if ReadWideString(FS,Lexems[i].Lexem)=False then
       exit(False);
    LexemNode:=New(PLexemNode);
    ZeroMemory(LexemNode,SizeOf(TLexemNode));
    LexemNode.LexemId:=Lexems[i].LexemId;
    LexemTreeInsert(LexemTree,LexemNode);
    end;
  exit(True);
end;

function ReadWords(FS:TFileStream):Boolean;
var i:integer;
  Count:Integer;
begin
  if FS.Read(Count,SizeOf(Count))<SizeOf(Count) then exit(False);
  SetLength(Words,Count);
  for i:=0 to Length(Words)-1 do
    if ReadWideString(FS,Words[i])=False then exit(False);
  exit(True);
end;

function ReadFilesOfWord(FS:TFileStream; var value:TFilesOfWord):Boolean;
var   WiD:Integer;
begin
  if FS.Read(WiD,SizeOf(WiD)) < SizeOf(WiD) then exit(False);
  value.GlobalWordId:=WiD;
  if ReadArraOfInt(FS,value.FileIdList)=false then exit(False);
  Exit(True);
end;

function ReadFilesOfLexem(FS:TFileStream; var value:TFilesOfLexem):Boolean;
var i:integer;
  Count:Integer;
begin
  if FS.Read(Count,SizeOf(Count))<SizeOf(Count) then exit(False);
  SetLength(value,Count);
  for i:=0 to Length(value)-1 do
    begin
     if ReadFilesOfWord(FS,value[i])=False then exit(False);
    end;
  Exit(True);
end;

function ReadLexemIndex(FS:TFileStream):Boolean;
var i:integer;
  Count:Integer;
begin
  if FS.Read(Count,SizeOf(Count))<SizeOf(Count) then exit(False);
  SetLength(LexemIndex,Count);
  for i:=0 to Length(LexemIndex)-1 do
    begin
     if ReadFilesOfLexem(FS,LexemIndex[i])=False then exit(False);
    end;
  exit(True);
end;

function ReadFilesName(FS:TFileStream):Boolean;
var i:integer;
  Count:Integer;
begin
  if FS.Read(Count,SizeOf(Count))<SizeOf(Count) then exit(False);
  SetLength(FilesName,Count);
  for i:=0 to Length(FilesName)-1 do
    if ReadWideString(FS,FilesName[i])=False then exit(False);
  exit(True);
end;

function LoadIndex(FileName:String):Boolean;
var FS:TFileStream;
begin
  FS:=TFileStream.Create(FileName,fmOpenRead);
  if ReadLexems(FS)=False then Exit(False);
  if ReadWords(FS)=False then Exit(False);
  if ReadLexemIndex(FS)=False then Exit(False);
  if ReadFilesName(FS)=False then Exit(False);
  FS.Free;
  Exit(True);
end;

function GetLexemId(const Value: WideString): integer;
var Node:PLexemNode;
begin
Node:=LexemTreeSearch(LexemTree,value);
if Node=nil then result:=-1
   else result:=Node.LexemId;
end;
{var
  i: integer;
begin
  result := -1;
  for i := 0 to Length(Lexems) - 1 do
    if Lexems[i].Lexem = Value then
    begin
      result := i;
      exit;
    end;
end;
}

function WordMap2Blob(WordMap: TWordMap; LengthBlob: integer): TArrayOfInt;
var
  i, j: integer;
  pos: integer;
begin
  SetLength(result, LengthBlob);
  for i := 0 to LengthBlob - 1 do
     result[i]:=-1;
  for i := 0 to Length(WordMap) - 1 do
  begin
    pos := 0;
    for j := 0 to Length(WordMap[i].Pozes) - 1 do
    begin
      pos := WordMap[i].Pozes[j];
      result[pos] := i;
    end;


  end;
end;

function WordMap2LengthBlob(WordMap: TWordMap): integer;
var
  i, j: integer;
  pos: integer;
begin
  result := -1;
  for i := 0 to Length(WordMap) - 1 do
  begin
    pos := 0;
    pos:=WordMap[i].Pozes[Length(WordMap[i].Pozes)-1];
    if pos > result then
      result := pos;
  end;
  result := result + 1;
end;

function ReadFilesDescriptorText(FileName: WideString): TFilesDescriptor;
var
  Text: TStringList;
  S: String;
  Splitted: TArray<String>;
  i, j: integer;
  pos:Integer;
begin
  Result.IndexFileName:=FileName;
  result.FileName := FileName;  //[!] считкать из файла

  Text := TStringList.Create;
  Text.LoadFromFile(FileName, TUTF8Encoding.Create);
  SetLength(result.WordMap, Text.Count);
  for i := 0 to Text.Count - 1 do
  begin
    S := Text[i];
    Splitted := S.Split([',']);
    result.WordMap[i].Value := Splitted[0];
    result.WordMap[i].Filtered := False;
    result.WordMap[i].LexemId := GetLexemId(result.WordMap[i].Value);

    SetLength(result.WordMap[i].Pozes, Length(Splitted)- 1);
    pos:=0;
    for j := 1 to Length(Splitted) - 1 do
      begin
      pos:=pos+StrToInt(Splitted[j]);
      result.WordMap[i].Pozes[j - 1] :=  pos;
      end;

  end;

  Text.free;

  result.LengthBlob := WordMap2LengthBlob(result.WordMap);
  result.Blob := WordMap2Blob(result.WordMap, result.LengthBlob);

end;

procedure OfsetToPozez(var Ofsets:TArrayOfInt);
var i:Integer;
 pos:Integer;
begin
  Pos:=0;
  for i:= 0 to Length(Ofsets)-1 do
    begin
    Pos:=Pos+Ofsets[i];
    Ofsets[i]:=Pos;
    end;
end;

function ReadFilesDescriptorBinary(FileName: WideString): TFilesDescriptor;
var Stream:TFileStream;
  Teg:array [0..3] of AnsiChar;
  Version:Int32;
  Count:Int32;
  I: Integer;
begin
  Result.IndexFileName:=FileName;
  Stream:=TFileStream.Create(FileName, fmOpenRead);
  Stream.Read(Teg,SizeOf(Teg));
  if Teg='idb_' then
     begin
     Stream.Read(Version,SizeOf(Version));
     if Version<>1 then raise Exception.Create('Файл создан в более поздней версии');

     ReadWideString(Stream,Result.FileName);
     Stream.Read(Count,SizeOf(Count));
     SetLength(Result.WordMap,Count);
     for I := 0 to Count-1 do
         begin
         ReadWideString(Stream,Result.WordMap[i].Value);
         result.WordMap[i].LexemId := GetLexemId(result.WordMap[i].Value);
         ReadArraOfInt(Stream,Result.WordMap[i].Pozes);
         OfsetToPozez(Result.WordMap[i].Pozes);
         Result.WordMap[i].Filtered:=False;
         end;
     end else raise Exception.Create('Неверный формат файла');

  Stream.free;

  result.LengthBlob := WordMap2LengthBlob(result.WordMap);
  result.Blob := WordMap2Blob(result.WordMap, result.LengthBlob);
end;

function ReadFilesDescriptor(FileName: WideString): TFilesDescriptor;
var Ext:WideString;
begin
  ext:=ExtractFileExt(FileName);
  if ext='.idb' then result:=ReadFilesDescriptorBinary(FileName)
     else result:=ReadFilesDescriptorText(FileName);

end;

function GetWordId(Value: WideString; FD: TFilesDescriptor): integer;
var
  i: integer;
begin
  result := -1;
  for i := 0 to Length(FD.WordMap) - 1 do
    if (Value = FD.WordMap[i].Value) then
    begin
      result := i;
      exit;
    end;
  Value:=UMorfer.DeleteEnding(UMorfer.ToLowerCase(value));
  for i := 0 to Length(FD.WordMap) - 1 do
    if (Value = UMorfer.DeleteEnding(UMorfer.ToLowerCase(FD.WordMap[i].Value))) then
    begin
      result := i;
      exit;
    end;

end;

function WordIdToWord(WordId: integer; FD: TFilesDescriptor): WideString;
begin
  result := '';
  if (0 <= WordId) and (WordId < Length(FD.WordMap)) then
    result := FD.WordMap[WordId].Value;
end;

function VectorItersect(A, B: TArrayOfInt): TArrayOfInt;
var
  i, j, k: integer;
  LenA, LenB: integer;
begin
  k := 0;
  i := 0;
  j := 0;
  LenA := Length(A);
  LenB := Length(B);
  SetLength(result, Min(LenA, LenB));
  while (i < LenA) and (j < LenB) do
  begin
    if A[i] > B[j] then
      inc(j)
    else if A[i] < B[j] then
      inc(i)
    else if A[i] = B[j] then
    begin
      result[k] := A[i];
      inc(k);
      inc(i);
      inc(j);
    end;
  end;
  SetLength(result, k);
end;

function VectorUnion(A, B: TArrayOfInt): TArrayOfInt;
var
  i, j, k: integer;
  LenA, LenB: integer;
begin
  k := 0;
  i := 0;
  j := 0;
  LenA := Length(A);
  LenB := Length(B);
  SetLength(result, LenA + LenB);
  while (i < LenA) and (j < LenB) do
  begin
    if A[i] > B[j] then
    begin
      result[k] := B[j];
      inc(k);
      inc(j);
    end
    else if A[i] < B[j] then
    begin
      result[k] := A[i];
      inc(k);
      inc(i);
    end
    else if A[i] = B[j] then
    begin
      result[k] := A[i];
      inc(k);
      inc(i);
      inc(j);
    end;
  end;
  while (i < LenA) do
  begin
    result[k] := A[i];
    inc(k);
    inc(i);
  end;
  while (j < LenB) do
  begin
    result[k] := B[j];
    inc(k);
    inc(j);
  end;
  SetLength(result, k);
end;

function GetPozesByLexem(LexemId:Integer; FD:TFilesDescriptor):TArrayOfInt;
var
  i: Integer;
begin
  result:=nil;
  for i := 0 to Length(FD.WordMap)-1 do
      if FD.WordMap[i].LexemId=LexemId then
         result:=VectorUnion(Result,FD.WordMap[i].Pozes);
end;

procedure Swap(var A,B:Integer);
var C:Integer;
begin
  C:=A;
  A:=B;
  B:=C;
end;
procedure TForm1.btnSearchClick(Sender: TObject);
var
  t1, t2: Int64;
  Splitted: TArray<String>;
  RowStr:String;
  Word1, Word2: WideString;
  WordId1, WordId2: integer;
  Lexem1, Lexem2: WideString;
  LexemId1, LexemId2: integer;
  FilesIDs,FilesIDs1,FilesIDs2:TArrayOfInt;
  Pozes1,Pozes2:TArrayOfInt;
  FD: TFilesDescriptor;
  i,j,k: integer;
  pos1, pos2: integer;
  S, Summer: WideString;
  dF:Integer;
const
 MaxSummerLen=10;
begin
  if IsIndexValidate=False then
     begin
     IsIndexValidate:=LoadIndex('..\..\data\index.dat');
     end;
  t1 := GetTickCount;

  RowStr:=Edit1.Text;
  Splitted:=RowStr.Split([' ']);

  Word1 := Splitted[0];
  Word2 := Splitted[2];

  Lexem1 := DeleteEnding(UMorfer.ToLowerCase(Word1));
  Lexem2 := DeleteEnding(UMorfer.ToLowerCase(Word2));

  LexemId1 := GetLexemId(Lexem1);
  LexemId2 := GetLexemId(Lexem2);

  if LexemId1<0 then exit;
  if LexemId2<0 then exit;

  FilesIDs1:=nil;FilesIDs2:=nil;
  for i := 0 to Length(LexemIndex[LexemId1]) - 1 do
  begin
   FilesIDs1:=VectorUnion(FilesIDs1,LexemIndex[LexemId1][i].FileIdList);
  end;


  for i := 0 to Length(LexemIndex[LexemId2]) - 1 do
  begin
   FilesIDs2:=VectorUnion(FilesIDs2,LexemIndex[LexemId2][i].FileIdList);
  end;

  FilesIDs:=VectorItersect(FilesIDs1,FilesIDs2);

  for k := 0 to Length(FilesIDs) - 1 do
     begin
       FD:=ReadFilesDescriptor(FilesName[k]);
       WordId1 := GetWordId(Word1, FD);
       WordId2 := GetWordId(Word2, FD);

       if (WordId1>=0) and (WordId2>=0)  then
          begin
          Pozes1:=FD.WordMap[WordId1].Pozes;
          Pozes2:=FD.WordMap[WordId2].Pozes;
          end else
          begin
          Pozes1:=GetPozesByLexem(LexemId1,FD);
          Pozes2:=GetPozesByLexem(LexemId2,FD);
          end;

       Memo1.Lines.Add(FD.FileName);

       pos1:=Pozes1[0];
       pos2:=0;
       j:=0;
       dF:=pos2-pos1;;
       for i := 0 to Length(Pozes2) - 1 do
         begin
         pos2:=Pozes2[i];
         while j < Length(Pozes1)-1 do
           begin

           if pos2<Pozes1[j] then
              begin
              break;
              end else
              begin
               pos1:=Pozes1[j];
               inc(j);
              end;
           end;
           df:=pos2-pos1;
           if (0<dF) and (dF<MaxSummerLen) then
              begin
              break;
              end;
         end;
       if df>MaxSummerLen then
          begin
          pos1:=Pozes1[0];
          pos2:=pos1+MaxSummerLen;
          end;


      Summer := '';
      for i := max(0, pos1 - 1) to min(Min(MaxSummerLen,pos2-pos1)+pos1+1, FD.LengthBlob) do
      begin
        S := WordIdToWord(FD.Blob[i], FD);
        Summer := Summer  + S;
        end;
      Memo1.Lines.Add(Summer);

     end;


  t2 := GetTickCount;
  Caption := IntToStr(t2 - t1);
end;

function GetWordGlobalId(Value: String): integer;
var
  i: integer;
begin
  result := -1;
  Value := UMorfer.ToLowerCase(Value);
  for i := 0 to Length(Words) - 1 do
    if Words[i] = Value then
    begin
      result := i;
      exit;
    end;
end;

// возвращает GlobalWordId
function AddWord(Value: WideString): integer;
begin
  result := GetWordGlobalId(Value);
  if result < 0 then
  begin
    SetLength(Words, Length(Words) + 1);
    result := Length(Words) - 1;
    Words[result] := Value;
  end;
end;

procedure AddLexem(var Lexem: TLexem);
var LexemNode:PLexemNode;
begin
  Lexem.LexemId := Length(Lexems);
  SetLength(Lexems, Length(Lexems) + 1);
  Lexems[Length(Lexems) - 1] := Lexem;
  LexemNode:=New(PLexemNode);
  ZeroMemory(LexemNode,SizeOf(TLexemNode));
  LexemNode.LexemId:=Lexem.LexemId;
  LexemTreeInsert(LexemTree,LexemNode);
end;

procedure AddFilesOfLexem(var FilesOfLexem: TFilesOfLexem;
  GlobalWordId: integer; FileId: integer);
var
  i: integer;
  WID: integer;
  Tail: integer;
begin
  WID := -1;
  for i := 0 to Length(FilesOfLexem) - 1 do
    if FilesOfLexem[i].GlobalWordId = GlobalWordId then
    begin
      WID := i;
      Break;
    end;
  if WID < 0 then
  begin
    SetLength(FilesOfLexem, Length(FilesOfLexem) + 1);
    WID := Length(FilesOfLexem) - 1;
    FilesOfLexem[WID].GlobalWordId := GlobalWordId;
  end;
  Tail := Length(FilesOfLexem[WID].FileIdList) - 1;
  if (Tail < 0) or (FilesOfLexem[WID].FileIdList[Tail] <> FileId) then
  begin
    inc(Tail);
    SetLength(FilesOfLexem[WID].FileIdList, Tail + 1);
    FilesOfLexem[WID].FileIdList[Tail] := FileId;
  end;

end;

procedure AddFileToIndex(FD: TFilesDescriptor; FileId: integer);
var
  i: integer;
  Lexem: TLexem;
  Word: WideString;
  GlobalWordId: integer;
begin
  SetLength(FilesName,Length(FilesName)+1);
  FilesName[Length(FilesName)-1]:=FD.IndexFileName;
  for i := 0 to Length(FD.WordMap) - 1 do
  begin
    Word := UMorfer.ToLowerCase(FD.WordMap[i].Value);
    GlobalWordId := AddWord(Word);
    Lexem.Lexem := DeleteEnding(Word);
    Lexem.LexemId := GetLexemId(Lexem.Lexem);
    if Lexem.LexemId < 0 then
    begin
      AddLexem(Lexem);
      SetLength(LexemIndex, Length(LexemIndex) + 1);
    end;
    AddFilesOfLexem(LexemIndex[Lexem.LexemId], GlobalWordId, FileId);
    FD.WordMap[i].LexemId:=Lexem.LexemId;
  end;
end;

procedure TForm1.btnRebuildIndexClick(Sender: TObject);
var
  FD: TFilesDescriptor;
begin
//  if (IsIndexValidate) then exit;

  SetLength(FilesDescriptor,3);
  FD := ReadFilesDescriptor('..\..\data\[Г. Лем] Аналоговые и цифровые фильтр. Рассчёт.idb');
  AddFileToIndex(FD, 0);
  FilesDescriptor[0]:=FD;
  FD := ReadFilesDescriptor('..\..\data\Окулов С.М.-Программирование в алгоритмах-БИНОМ. Лаборатория знаний (2014).idb');
  AddFileToIndex(FD, 1);
  FilesDescriptor[1]:=FD;
  FD := ReadFilesDescriptor('..\..\data\Юрий Олеша-Три толстяка-Детская литература (1976).idb');
  AddFileToIndex(FD, 2);
  FilesDescriptor[2]:=FD;
  IsIndexValidate:=True;
end;

procedure TForm1.btnSaveIndexClick(Sender: TObject);
begin
  SaveIndex('..\..\data\index.dat');
end;


end.

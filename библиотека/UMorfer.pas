﻿unit UMorfer;

 {$IFDEF FPC}
{$Mode delphiunicode}

{$codepage utf8}

{$ENDIF}

interface

function ToLowerCase(value: String): String;
function DeleteEnding(s: String): String;

implementation

// Гласные
function IsGlasnyye(const ch: WideChar): Boolean;
begin
  Result := (('А' = ch) or ('У' = ch) or ('О' = ch) or ('Ы' = ch) or ('Э' = ch)
    or ('Я' = ch) or ('Ю' = ch) or ('Ё' = ch) or ('И' = ch) or ('Е' = ch)) or
    (('а' = ch) or ('у' = ch) or ('о' = ch) or ('ы' = ch) or ('э' = ch) or
    ('я' = ch) or ('ю' = ch) or ('ё' = ch) or ('и' = ch) or ('е' = ch));
end;

function isUpAlfa(const ch: WideChar): Boolean;
begin
  Result := (('А' <= ch) and (ch <= 'Я')) or (ch = 'Ё') or
    (('A' <= ch) and (ch <= 'Z'));
end;

{$IFDEF FPC}
function ToLowerCase(value: String): String;
var
  i: Integer;
  ch: WideChar;
begin
  SetLength(Result, Length(value));
  for i := Low(value) to High(value) do
  begin
    ch := value[i];
    if (#$410{'А'} <= ch) and (ch <= #$42F{'Я'}) then
      ch := WideChar($430{'а'} + (Ord(ch) - $410{'А'}));
    if (#$401{'Ё'} = ch) then
      ch := #$451{'ё'};
    if (#65{'A'} <= ch) and (ch <= #90{'Z'}) then
      ch := WideChar(97{'a'} + (Ord(ch) -65{'A'}));
    Result[i] := ch;
  end;
end;
{$ELSE}
function ToLowerCase(value: String): String;
var
  i: Integer;
  ch: WideChar;
begin
  SetLength(Result, Length(value));
  for i := Low(value) to High(value) do
  begin
    ch := value[i];
    if ('А' <= ch) and (ch <= 'Я') then
      ch := WideChar(Ord('а') + (Ord(ch) - Ord('А')));
    if ('Ё' = ch) then
      ch := 'ё';
    if ('A' <= ch) and (ch <= 'Z') then
      ch := WideChar(Ord('a') + (Ord(ch) - Ord('A')));
    Result[i] := ch;
  end;
end;

{$IFEND}

// Получаем окончание слова.
// Если начинается с гласной, то идем до согласной или беззвучной
// Если начинается с согласной, то идем до гласной
// [!] Подумать как улучшить алгоритм
function GetFormalEnding(s: String): String;
var
  i, j: Integer;
begin
  if (s = '') then
    Result := ''
  else if (s[Length(s)] = '-') then
    Result := ''
  else if IsGlasnyye(s[Length(s)]) then
  begin
    j := 1;
    for i := Length(s) downto 1 do
      if not(IsGlasnyye(s[i])) then
      begin
        j := i + 1;
        Break;
      end;
    Result := copy(s, j, Length(s) - j + 1);
  end
  else
  begin
    j := 1;
    for i := Length(s) downto 1 do
      if (IsGlasnyye(s[i])) then
      begin
        j := i;
        Break;
      end;
    Result := copy(s, j, Length(s) - j + 1);
  end;

end;

function DeleteEnding(s: String): String;
var
  LenEnd: Integer;
begin
  if Length(s) <= 3 then
    Result := s
  else
  begin
    LenEnd := Length(GetFormalEnding(s));
    Result := s;
    Delete(Result, Length(Result) - LenEnd + 1, LenEnd);
    if Result = '' then
      Result := s;
  end;
end;

end.

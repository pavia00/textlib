unit USegments;

interface

type
  TPoint1D=Int64;
  TMetric=Int64;

  TSegment=record
      Start:TPoint1D;
      Length:TMetric;
    end;
  TItrSegm=(isNo, IsAInB, isBOutLeft, IsBOutRight, IsBInA);

function LeftPoint(const A:TSegment):TPoint1D;
function RightPoint(const A:TSegment):TPoint1D;

function IntersectionSegments(var Goal:TItrSegm; A,B:TSegment):Boolean;
function IntersectionSegmentPont(const A:TSegment; const Point:TPoint1D):Boolean;

function Segment(const Start, Length:TMetric):TSegment;

implementation

function LeftPoint(const A:TSegment):TPoint1D;
begin
Result:=A.Start;
end;

function RightPoint(const A:TSegment):TPoint1D;
begin
Result:=A.Start+A.Length-1;
end;

function IntersectionSegmentPont(const A:TSegment; const Point:TPoint1D):Boolean;
begin
Result:=(LeftPoint(A)<=Point) and (Point<RightPoint(A));
end;

function IntersectionSegments(var Goal:TItrSegm; A,B:TSegment):Boolean;
var
  Left, Right:boolean;
begin
Left:=IntersectionSegmentPont(A, RightPoint(B));
Right:=IntersectionSegmentPont(A, LeftPoint(B));
Goal:=IsNo;
If (Left=True) and (Right=True) then
    Goal:=isBInA;
If (Left=True) and (Right=False) then
    Goal:=isBOutLeft;
If (Left=False) and (Right=True) then
    Goal:=isBOutRight;
If (Left=False) and (Right=False) then
   if IntersectionSegmentPont(B, LeftPoint(A)) then
        Goal:=IsAInB
      else
        Goal:=IsNo;

if Goal=IsNo then
     Result:=False
   else
     Result:=True;
end;

function Segment(const Start, Length:TMetric):TSegment;
begin
  Result.Start:=Start;
  Result.Length:=Length;
end;

end.

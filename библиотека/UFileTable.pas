unit UFileTable;

interface

uses Classes, UBufferedStream;

type
  TTableProc= procedure (Idx:Int64; Item:Pointer);
  TIndexHead=packed record  {64 �����}
    Version:Cardinal;
    Count:Int64;
    CountUsed:Int64;
    Reserv:array [1..44] of byte;
  end;

  TIndexRec=packed record  {32 �����}
    Idx:Int64;
    Ofs:Int64;
    Deleted:Boolean;
    Reserv:array [1..15] of byte;
  end;

  TIndexProc=procedure (Idx:Int64;var Item:TIndexRec);

  TFileIndex = Class (TBufferedStream)
  private
    FHead:TIndexHead;
    function GetCount: Int64;
    procedure SetCount(const Value: Int64);
  public
    property  Count:Int64 read GetCount Write SetCount;
    procedure GetItem(Idx:Int64; var Item:TIndexRec);
    procedure SetItem(Idx:Int64; Item:TIndexRec);
    procedure AppItem(Item:TIndexRec);
    procedure DeleteItem(Idx:Int64);
    procedure ForEach(proc:TIndexProc);
    procedure WriteHead;
    procedure Compresia;
  end;

  TFileTable = class
  private
    FName:String;
    FSTable:TFileStream;
    FSIndex:TFileStream;
    FCount:Int64;
  protected

  public
    Procedure Assign(Value:String);
    Procedure New;
    Procedure Open;
    Procedure Close;
    function  Count:Int64;
    procedure GetItem(Idx:Int64; Item:Pointer);
    procedure SetItem(Idx:Int64; Item:Pointer);
    procedure AppItem(Item:Pointer);
    procedure DeleteItem(Idx:Int64);
    procedure ForEach(proc:TTableProc);
    constructor Create;
    destructor Destroy;
  published

  end;


implementation

{ TFileTable }

procedure TFileTable.AppItem(Item: Pointer);
begin
Inc(FCount);

end;

procedure TFileTable.Assign(Value: String);
begin
  FName:=Value;
end;

procedure TFileTable.Close;
begin

end;

function TFileTable.Count: Int64;
begin

end;

constructor TFileTable.Create;
begin
  inherited;

end;

procedure TFileTable.DeleteItem(Idx: Int64);
begin

end;

destructor TFileTable.Destroy;
begin
  FSIndex.Destroy;
  FSTable.Destroy;
  inherited;
end;

procedure TFileTable.ForEach(proc: TTableProc);
begin

end;

procedure TFileTable.GetItem(Idx: Int64; Item: Pointer);
begin

end;

procedure TFileTable.New;
begin
{  FSTable:= TFileStream.Create(FName+'.��', fmCreate);
  FSIndex:= TFileStream.Create(FName+'.��', fmCreate);  }
end;

procedure TFileTable.Open;
begin
{  FSTable:= TFileStream.Create(FName+'.��', fmOpenReadWrite);
  FSIndex:= TFileStream.Create(FName+'.��', fmOpenReadWrite);}
end;

procedure TFileTable.SetItem(Idx: Int64; Item: Pointer);
begin

end;

{ TFileIndex }

procedure TFileIndex.AppItem(Item: TIndexRec);
begin
  Count:=Count+1;
  SetItem(Count,Item);
end;

procedure TFileIndex.Compresia;
var
 TheCount:Int64;
 I,K:Int64;
 Item:TIndexRec;
begin
  I:=0;
  K:=0;
  TheCount:=Count;
  {�����������, ���������� ������ �� ����������}
  while I<TheCount do
    begin
      GetItem(I,Item);
      if Item.Deleted then
         Break;
      K:=K+1;
      I:=I+1;
    end;
  {�������� ���}
  while I<TheCount do
    begin
      GetItem(I,Item);
      if Not Item.Deleted then
         begin
           SetItem(K,Item);
           K:=K+1;
         end;
     I:=I+1;
    end;
  Self.Size:=Self.Position; {�������� ����� �����}
end;


procedure TFileIndex.DeleteItem(Idx: Int64);
var
 Item:TIndexRec;
begin
  GetItem(idx,Item);
  Item.Deleted:=True;
  SetItem(idx,Item);
end;

procedure TFileIndex.WriteHead;
begin
   Seek(0,soBeginning);
   WriteBuffer(FHead, SizeOf(FHead));
end;

procedure TFileIndex.ForEach(proc: TIndexProc);
var
 I:int64;
 TheCount:Int64;
 Item:TIndexRec;
begin
  I:=0;
  TheCount:=Count;
  while I<TheCount do
    begin
     GetItem(I,Item);
     Proc(I, Item);
     SetItem(I,Item);
    end;
end;

function TFileIndex.GetCount: Int64;
begin
 Result:=FHead.Count;
end;

procedure TFileIndex.GetItem(Idx: Int64; var Item: TIndexRec);
begin
if Idx>=0 then
   begin
   Seek(Int64(SizeOf(FHead))+SizeOf(Item)*Idx, soBeginning);
   ReadBuffer(Item, SizeOf(Item));
   end else
   begin
      Item.Idx:=-1;
      Item.Ofs:=-1;
      Item.Deleted:=True;
   end;
end;

procedure TFileIndex.SetCount(const Value: Int64);
begin
   FHead.Count:=Value;
end;

procedure TFileIndex.SetItem(Idx: Int64; Item: TIndexRec);
var
 ThePos:Int64;
begin
if Idx>=0 then
   begin
   ThePos:=Int64(SizeOf(FHead))+SizeOf(Item)*Idx;
   Seek(ThePos, soBeginning);
   WriteBuffer(Item, SizeOf(Item));
   end;
end;

end.

unit UBufferedStream;
{�������� TBufferedStream.Write ��� �������� �� ����� � ������ �������� ���������������� ������ ��� ������ ����������}
interface

uses
  Classes, SysUtils, Math, USegments;

type
  TBufferedStream = class(TStream)
  private
    FStream: TStream;
    FOwnership: TStreamOwnership;
    FPosition: Int64;
    FBuff: array of byte;
    FBuffStartPosition: Int64;
    FBuffSize: Integer;
    FBuffMaxSize: Integer;
    procedure SetBufferSize(Value: Integer);
    procedure SetBufferMaxSize(Value: Integer);
    function GetBuffer_Segment: TSegment;
  protected
    property Buffer_StartPosition: Int64 read FBuffStartPosition;
    function Buffer_Read(var Buffer; Size: LongInt): Longint;
    function Buffer_Update: Boolean;
    function Buffer_Contains(APosition: Int64): Boolean;
    property Buffer_Segment: TSegment read GetBuffer_Segment;
  public
    constructor Create(AStream: TStream; AOwnership: TStreamOwnership = soReference); overload;
    constructor Create(const AFileName: string; Mode: Word; ABuffMaxSize: Integer = 1024 * 1024); overload;
    destructor Destroy; override;
    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const ABuffer; Count: Longint): Longint; override;
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
    property BufferSize: Integer read FBuffSize;
    property BufferMaxSize: Integer read FBuffMaxSize write SetBufferMaxSize;
    procedure Flush;
    procedure InvalidateBuffer;
  end;

implementation

{ TBufferedStream }


function TBufferedStream.Buffer_Contains(APosition: Int64): Boolean;
begin
  Result :=IntersectionSegmentPont(Buffer_Segment, APosition);
end;

function TBufferedStream.Buffer_Read(var Buffer; Size: LongInt): Longint;
begin
  Result := 0;
  if not Buffer_Contains(FPosition) then Exit;
  Result := Buffer_Segment.Length;
  if Result > Size then
    Result := Size;
  Move(FBuff[Integer(FPosition - Buffer_StartPosition)], Buffer, Result);
  Inc(FPosition, Result);
end;

function TBufferedStream.Buffer_Update: Boolean;
var
  Readed:Integer;
begin
  FStream.Position := FPosition;
  FBuffStartPosition := FPosition;
  SetLength(FBuff, FBuffMaxSize);
  Readed:=FStream.Read(FBuff[0], FBuffSize);
  FBuffSize:=Readed;
  Result := Length(FBuff) > 0
end;

constructor TBufferedStream.Create(AStream: TStream; AOwnership: TStreamOwnership = soReference);
begin
  FStream := AStream;
  FOwnership := AOwnership;
  InvalidateBuffer;
  BufferMaxSize := 1024 * 1024;
  Buffer_Update;
end;

constructor TBufferedStream.Create(const AFileName: string; Mode: Word; ABuffMaxSize: Integer);
begin
  Create(TFileStream.Create(AFileName, Mode), soOwned);
  BufferMaxSize := ABuffMaxSize;
end;

destructor TBufferedStream.Destroy;
begin
  Flush;
  if FOwnership = soOwned then
    FreeAndNil(FStream);
  inherited;
end;

procedure TBufferedStream.InvalidateBuffer;
begin
  FBuff := nil
end;

function TBufferedStream.Read(var Buffer; Count: Longint): Longint;
var
  Readed: Integer;
begin
  Result := 0;
  while Result < Count do
  begin
    Readed := Buffer_Read(PAnsiChar(@Buffer)[Result], Count - Result);
    Inc(Result, Readed);
    if Readed = 0 then
      if not Buffer_Update then
        Exit;
  end;
end;

function TBufferedStream.Seek(const Offset: Int64;
  Origin: TSeekOrigin): Int64;
begin
  case Origin of
    soBeginning: FPosition := Offset;
    soCurrent: Inc(FPosition, Offset);
    soEnd: FPosition := FStream.Size + Offset;
  end;
  Result := FPosition;
  if not Buffer_Contains(FPosition) then
    InvalidateBuffer;
end;

procedure TBufferedStream.SetBufferSize(Value: Integer);
begin
  if Value < 0 then
    Value := 16 * 1024;
  if FBuffSize <> Value then
  begin
    FBuffSize := Value;
    InvalidateBuffer;
  end;
end;

procedure TBufferedStream.SetBufferMaxSize(Value: Integer);
begin
  if Value < 0 then
    Value := 16 * 1024*1024;
  if FBuffMaxSize <> Value then
  begin
    FBuffMaxSize := Value;
    SetBufferSize(Value);
  end;
end;

function TBufferedStream.Write(const ABuffer; Count: Integer): Longint;
  Procedure ReChangBuf;
  begin
    Flush;
    FStream.Seek(Position, soBeginning);
    FStream.Write(ABuffer, Count);
    Position:=Position+Count;
    Buffer_Update;
  end;
  function TryResizeBuffer:Boolean;
  Var
    NewLength:Integer;
  begin
  Result:=False;
  NewLength:=Buffer_Segment.Length+Count;
  if (NewLength<=Length(FBuff)) then
      begin
       FBuffSize:=NewLength;
       Result:=True;
      end
      else
      if (NewLength<=FBuffMaxSize) then
         begin
         SetLength(FBuff, NewLength);
         FBuffSize:=NewLength;
         Result:=True;
         end;
  end;
  Procedure CopyInBuf;
  var
    Pos:Int64;
  begin
    Pos:=FPosition- FBuffStartPosition;
    Move(ABuffer, FBuff[Pos] , Count);
    FPosition:=FPosition+Count;
  end;
var
  ItrSegm:TItrSegm;
begin
  if FBuff=nil then Buffer_Update;
  IntersectionSegments(ItrSegm, Buffer_Segment, Segment(FPosition, Count));
  case ItrSegm of
  isNo:
    begin
     if (RightPoint(Buffer_Segment)+1=FPosition) and
        (TryResizeBuffer) then
           begin
             CopyInBuf;
           end else
             ReChangBuf;
    end;
  IsAInB:     ReChangBuf;
  isBOutLeft: ReChangBuf;
  IsBOutRight:
    begin
     if RightPoint(Buffer_Segment)+1=FPosition then
        if TryResizeBuffer then
           begin
             CopyInBuf;
           end else
             ReChangBuf;
    end;
  IsBInA: ReChangBuf;
  end; // case
end;

procedure TBufferedStream.Flush;
begin
   FStream.Seek(FBuffStartPosition, soBeginning);
   FStream.Write(FBuff[0], BufferSize);
end;

function TBufferedStream.GetBuffer_Segment: TSegment;
begin
Result.Start:=FBuffStartPosition;
Result.Length:=FBuffSize;

end;

end.


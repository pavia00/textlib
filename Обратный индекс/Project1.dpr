program Project1;

{$APPTYPE CONSOLE}

uses
  Windows,
  SysUtils,
  Classes,
  RegularExpressionsCore,
  System.IOUtils,
  System.Generics.Defaults,
  System.Generics.Collections;

var
  Stream:TBytesStream;

{PS - ���� ���������� ����������
 LS - ���� ���������� ������
 TB - ���� ���������
 NL - ������� ������ }
const
 PS:WideChar=#$2029;
 LS:WideChar=#$2028;
 TB:WideChar=#09;
 NL:String=#13#10;

type
   {���� ������}
   TSimvolKind=(skNil, skOther,skStartOfFile, skEndOfFile,skSpace, skWord, skNumber, skString, skPunctuator, skComment);
   TSimvol=record
     Value:String;
     Kind:TSimvolKind;
     end;

 TSimvols=TList<TSimvol>;

 TText=record
   FFofs:Int64;
   ofs:Int64;
   FPos:Int64;
   FText:String;
   FCurrentSimvol:TSimvol;
   FNextSimvol:TSimvol;
   end;
 TWordIndex=record
    Word:String;
    pos:TList<Integer>;
 end;
 TInvertDocument=record
   FileName:WideString;
   Words:array of TWordIndex;
 end;
 TDocumentDic=TDictionary<String,TList<Integer>>;



{����������� ���� ������}
const
 SimvolNil:TSimvol=(Value:''; Kind:skNil);
 SimvolSOT:TSimvol=(Value:''; Kind:skStartOfFile); // SOT - Start of text
 SimvolEOT:TSimvol=(Value:''; Kind:skEndOfFile);   // EOT - End of text

//
function ReadNextSimvol(var Text:TText):TSimvol; forward;
function ReadWord(var Text:TText):TSimvol; forward;

{���������� ��������� �� ������}
procedure Reset(var f :TText);
begin
f.ofs:=1;
f.FFofs:=1;
F.FPos:=1;
f.FCurrentSimvol:=ReadNextSimvol(F);
f.FNextSimvol:=ReadNextSimvol(F);
end;


procedure Assign(var f :TText; Text:String);
begin
F.FText:=Text;
end;

{������� �������� ����� �����}
function EOT(const f:TText):Boolean;
begin
result:= f.ofs>Length(f.FText);
end;

{������� �������� ������ �����}
function SOT(const f:TText):Boolean;
begin
result:= f.ofs=1;
end;

{��������� ������� ������}
function CurrentChar(const f:TText):WideChar;
begin
Result:=#0;
if (0<f.ofs) and (f.ofs<=Length(f.FText)) then
   result:= f.FText[f.ofs];
end;

{��������� ��������� ������ ��� �������� ���������}
function NextChar(const f:TText):WideChar;
var Ofs1:Int64;
begin
Result:=#0;
Ofs1:=f.ofs+1;
if (0<Ofs1) and (Ofs1<=Length(f.FText)) then
   result:= f.FText[Ofs1];
end;

{���������� ��������� ������ ��� ���� ������� ��������� �� ��������� ������}
function ReadChar(var f:TText):WideChar;
begin
Result:=#0;
if (0<f.ofs) and (f.ofs<=Length(f.FText)) then
   begin
   result:= f.FText[f.ofs];
   inc(f.ofs);
   end;
end;

function FCurrentChar(const f:TText):WideChar;
begin
Result:=#0;
if (0<f.FFofs) and (f.FFofs<=Length(f.FText)) then
   result:= f.FText[f.FFofs];
end;

function FNextChar(const f:TText):WideChar;
var Ofs1:Int64;
begin
Result:=#0;
Ofs1:=f.FFofs+1;
if (0<Ofs1) and (Ofs1<=Length(f.FText)) then
   result:= f.FText[Ofs1];
end;

function FReadChar(var f:TText):WideChar;
begin
Result:=#0;
if (0<f.FFofs) and (f.FFofs<=Length(f.FText)) then
   begin
   result:= f.FText[f.FFofs];
   inc(f.FFofs);
   f.ofs:=f.FFofs;
   end;
end;


function FEOT(const f:TText):Boolean;
begin
result:= f.FFofs>Length(f.FText);
end;

function FSOT(const f:TText):Boolean;
begin
result:= f.FFofs=1;
end;

function IsAlfa(const ch:WideChar):Boolean;
begin
Result:=(('�'<=Ch) and (CH<='�') ) or (Ch='�') or
        (('�'<=Ch) and (CH<='�') ) or (Ch='�') or
        (('A'<=Ch) and (CH<='Z') ) or
        (('a'<=Ch) and (CH<='z') );
end;

function isUpAlfa(const ch:WideChar):Boolean;
begin
Result:=(('�'<=Ch) and (CH<='�') ) or (Ch='�') or
        (('A'<=Ch) and (CH<='Z') );
end;


function IsWordAlfa(const ch:WideChar):Boolean;
begin
Result:=IsAlfa(ch);
result:=Result or ('_'=Ch) or ('-'=Ch) or ('�'=Ch);
end;

function IsDigital(const ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, ['0'..'9']);
end;

function IsNewLineChar(const ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, [#10, #13]);
Result:= Result or (Ch=LS) or (Ch=PS);
end;

function IsSpace(const ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, [#9, #10, #11, #12, #13, #32]);
Result:= Result or IsNewLineChar(ch);
end;

function IsPunctuation(const ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, ['!', '"', '#', '$', '%', '&', '''', '(', ')', '*',
                       '+', ',', '-', '.', '/', ':', ';', '<', '=', '>',
                       '?', '@', '[', '\', ']', '^', '`', '{', '|', '}',
                       '~']);

end;

function IsOther(ch:WideChar):Boolean;
begin
Result:=Not (IsDigital(ch) or IsWordAlfa(ch) or IsSpace(ch) or IsPunctuation(ch));
end;

function IsUri_gen_delims(ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, [':', '/', '?', '#', '[', ']', '@']);
end;

function IsUri_sub_delims(ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, ['!', '$', '''', '(', ')', '*', '+', ',', ';', '=']);
end;

function IsUri_reserved(ch:WideChar):Boolean;
begin
Result:=IsUri_gen_delims(ch) or IsUri_sub_delims(ch);
end;

function IsUri_unreserved(ch:WideChar):Boolean;
begin
Result:=IsAlfa(Ch) or IsDigital(Ch);
Result:=Result or CharInSet(Ch, ['-', '.', '_', '~']);
end;

function IsUriChar(ch:WideChar):Boolean;
begin
Result:=IsUri_unreserved(Ch) or
        CharInSet(Ch,['%', ';', '?', ':', '@', '&', '=', '+', '$', ',', '/']);

end;

// �������
function IsGlasnyye(const ch:WideChar):Boolean;
begin
Result:=(('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or
           ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch)) or
        (('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or
           ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch));
end;

// ���������
function IsSoglasnyye(const ch:WideChar):Boolean;
begin
Result:=(('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or
           ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or
           ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or
           ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or
           ('�'=Ch) ) or
        (('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or
           ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or
           ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or
           ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or ('�'=Ch) or
           ('�'=Ch) );
end;

// ����������
function IsBezzvuchnyye(const ch:WideChar):Boolean;
begin
Result:=(('�'=Ch) or (CH='�') ) or
        (('�'=Ch) or (CH='�') );
end;

function ReadNumber(var Text:TText): TSimvol;
var
  s:WideString;
begin
 S:='';
 while IsDigital(FCurrentChar(Text)) or (FCurrentChar(Text)='.') do
   begin
     S:=S+FReadChar(Text);
   end;
 Result.value:=s;
 Result.Kind:=skNumber;
end;

function ReadOther(var Text:TText): TSimvol;
var
  s:WideString;
begin
 S:='';
 while IsOther(FCurrentChar(Text)) and (not FEOT(Text)) do
   begin
     S:=S+FReadChar(Text);
   end;
 Result.value:=s;
 Result.Kind:=skOther;
end;

function ReadPunctuator(var Text:TText): TSimvol;
var
  s:String;
  ch1,ch2:WideChar;
begin
 S:='';
 if IsPunctuation(FCurrentChar(Text)) then
     begin
     ch1:=FReadChar(Text);
     S:=S+Ch1;
     Ch2:=FCurrentChar(Text);
     if IsPunctuation(Ch2) then
         begin

         if ((ch1='<') and (ch2='>')) or
            ((ch1='<') and (ch2='=')) or
            ((ch1='>') and (ch2='=')) or
            ((ch1=':') and (ch2='=')) then
             begin
             Ch2:=FReadChar(Text);
             S:=s+Ch2;
             end;
         end;
     end;

 Result.value:=s;
 Result.Kind:=skPunctuator;
end;

function ReadNewLine(var Text:TText):TSimvol;
var
  s:String;
begin
 S:='';
 if IsNewLineChar(FCurrentChar(Text)) then
   begin
   if FCurrentChar(Text)=#13 then
    begin
    S:=S+FReadChar(Text);
    if FCurrentChar(Text)=#10 then
       S:=S+FReadChar(Text);
    end else S:=S+FReadChar(Text);
   end;
 Result.value:=s;
 Result.Kind:=skSpace;
end;

function ReadSpace(var Text:TText): TSimvol;
var
  s:String;
begin
 S:='';
 if IsNewLineChar(FCurrentChar(Text)) then
    begin
    Result:=ReadNewLine(Text);
    exit;
    end
    else
    begin
    S:=FReadChar(Text);
    while IsSpace(FCurrentChar(Text))and (not IsNewLineChar(FCurrentChar(Text)) ) do
     begin
     S:=S+FReadChar(Text);
     end;
   end;
 Result.value:=s;
 Result.Kind:=skSpace;
end;

function ReadEot(var Text:TText): TSimvol;
begin
 Result:=SimvolEOT;
end;

{��������� ������������ ����� ���������}
{������������ ��������� ��������� �������� �� Text}
function IsHyphen(Word:String; var Text:TText):boolean;
begin
Result:=False;
 if (Word[Length(Word)]='-') or (Word[Length(Word)]='�') then
      if IsNewLineChar(FCurrentChar(Text)) then
        Result:=True;
end;

{���������� ������� � ������ �������� �����}
{������� ������ �������� � �������� ��������, �������� ����� ������� ��� ��������}
procedure ReadHyphenWord(var s:String; var Text:TText);
var SecondWord:TSimvol;
begin
 Delete(s, Length(s),1);
 ReadNewLine(Text);
 SecondWord:=ReadWord(Text);
 S:=S+SecondWord.Value;
end;

function ReadWord(var Text:TText):TSimvol;
var
  s:String;
begin
 S:='';
 if IsWordAlfa(FCurrentChar(Text)) then
   begin
   if (FCurrentChar(Text)='-') then
       S:=S+FReadChar(Text)
     else while (IsWordAlfa(FCurrentChar(Text))) or (IsDigital(FCurrentChar(Text))) do
     begin
       S:=S+FReadChar(Text);
     end;
     {�������}
     if IsHyphen(s, Text) then
          ReadHyphenWord(s,Text);
   end;
 Result.value:=s;
 Result.Kind:=skWord;
end;

function ReadNextSimvol(var Text:TText):TSimvol;
 function ReadStartFromPunctuation(var Text:TText):TSimvol;
 begin
   Result:=ReadPunctuator(Text);
 end;
var
  Ch:WideChar;
begin
  Ch:=FCurrentChar(Text);
  if IsWordAlfa(Ch) then Result:=ReadWord(Text)
     else if IsSpace(Ch) then Result:=ReadSpace(Text)
        else if IsDigital(Ch) then Result:=ReadNumber(Text)
           else if IsPunctuation(Ch) then Result:=ReadStartFromPunctuation(Text)
                else Result:=ReadOther(Text);
  if FEot(Text) then Result:=ReadEot(Text);

end;

function ReadSimvol(var Text:TText): TSimvol;
begin
 Result:=Text.FCurrentSimvol;
 Text.FCurrentSimvol:=Text.FNextSimvol;
 Text.FNextSimvol:=ReadNextSimvol(Text);
 Inc(Text.FPos);
end;


function CompareSimvov(const Left, Right: TSimvol): Integer;
begin
  if Left.Value<Right.Value then Result:=-1
     else if Left.Value=Right.Value then Result:=0
       else Result:=+1
end;

{����������� ����� ����� � ������ ��������}
function LexPars(Text:String):TSimvols;
var
  t:TText;
begin
Result:=TList<TSimvol>.Create(TComparer<TSimvol>.Construct(CompareSimvov)); // ������� ����� �������� � �������� ��������� ��� ����������
Result.Add(SimvolSOT);

 Assign(T,Text);
 Reset(T);
 repeat
 Result.Add(ReadSimvol(T));
 until EOT(T);
 Result.Add(ReadSimvol(T));

end;

{������������� ����������. ���������� ����������� � ������ � ������.
{������������ ��������� �� ���������� �������� ���������� ����� ��������}
{����� ��������� ���������� ��� �������� �� ������ �������� ����� ����� ������� �� ���������� ����� �����������. � �������� ��� ���������� ����}
{����� ������� �� ������� � ������ ���� ��������}
procedure PrepAbbr(var simvols:TSimvols);
var
  i:Integer;
  ForDelete:TList<integer>;
  Abrr:Boolean;
  Simvol_1:TSimvol;
  s:String;
  procedure CheckDelete(i:Integer);
  var
    Simvol__1, Simvol__2:TSimvol;
  begin
   if i+1<=Simvols.Count-1 then   Simvol__1:=Simvols[i+1] else Simvol__1:=SimvolNil;
   if i+2<=Simvols.Count-1 then Simvol__2:=Simvols[i+2] else Simvol__2:=SimvolNil;
   if (Simvol__1.Kind=skSpace) and
       (Length(Simvol__2.Value)>=1) and
       not isUpAlfa(Simvol__2.Value[1]) then
       ForDelete.Add(i);
   if  (Length(Simvol__1.Value)>=1) and
       not isUpAlfa(Simvol__1.Value[1]) then
       ForDelete.Add(i);

  end;
begin
ForDelete:=TList<integer>.Create;
for i:=1 to Simvols.Count-1 do
  begin
  if (Simvols[i].Value='.') then
     begin
     Simvol_1:=Simvols[i-1];
     s:=Simvol_1.Value;
     Abrr:=False;
     if (S='�') then Abrr:=True;   //�.�. - �� ����
     if (S='�') then Abrr:=True;   //�.�. - �� ����
     if (S='�') then Abrr:=True;   //�.�. - ��� �����
     if (S='�') then Abrr:=True;   //�.�. - ���� ��������
     if (S='�') then Abrr:=True;   //�.�. - ����� ������
     if (S='�') then Abrr:=True;   //�. - ���
     if (S='��') then Abrr:=True;  //��. - ����
     if (S='�') then Abrr:=True;   //�. - �������
     if (S='�') then Abrr:=True;   //�.�. - ����� ������
     if (S='�') then Abrr:=True;   //�. - ������
     if (S='��') then Abrr:=True;  //��. - ����������
     if (S='��') then Abrr:=True;  //��. - ����
     if (S='��') then Abrr:=True;  //��. - ����
     if (S='�') then Abrr:=True;   //�. - ������
     if (S='���') then Abrr:=True; //���. - ������
     if (S='���') then Abrr:=True; //���. - �������
     if (S='���') then Abrr:=True; //���. - �������
     if (S='���') then Abrr:=True; //���. - �������

     if Abrr then
        begin
        Simvol_1.Value:=Simvol_1.Value+'.';
        Simvols[i-1]:=Simvol_1;
        CheckDelete(i);
        end;
     end;
  end;
for i:=ForDelete.Count-1 downto 0 do
   Simvols.Delete(ForDelete[i]);

ForDelete.Clear;

for i:=1 to Simvols.Count-1 do
  begin
  if (Simvols[i-1].Value='�.') and (Simvols[i].Value='�.') then
     begin
     Simvol_1:=Simvols[i-1];
     Simvol_1.Value:='�.�.';
     Simvols[i-1]:=Simvol_1;
     ForDelete.Add(i);
     end;
  if (Simvols[i-1].Value='�.') and (Simvols[i].Value='�.') then
     begin
     Simvol_1:=Simvols[i-1];
     Simvol_1.Value:='�.�.';
     Simvols[i-1]:=Simvol_1;
     ForDelete.Add(i);
     end;
  if (Simvols[i-1].Value='�.') and (Simvols[i].Value='�.') then
     begin
     Simvol_1:=Simvols[i-1];
     Simvol_1.Value:='�.�.';
     Simvols[i-1]:=Simvol_1;
     ForDelete.Add(i);
     end;
  if (Simvols[i-1].Value='�.') and (Simvols[i].Value='�.') then
     begin
     Simvol_1:=Simvols[i-1];
     Simvol_1.Value:='�.�.';
     Simvols[i-1]:=Simvol_1;
     ForDelete.Add(i);
     end;
  end;

for i:=ForDelete.Count-1 downto 0 do
   Simvols.Delete(ForDelete[i]);
ForDelete.Destroy;
end;

{������� �������������� ����� ���� P1,I75, � ��� �� ����������
������ ���� ��������� ����� ���������� ���������� � ��������� ����}
procedure DeleteNotWord(var Simvols:TSimvols);
var
 NewArray:TSimvols;
 i:Integer;
 Simvol:TSimvol;
 NewStatement:Boolean;
 procedure MakeDown(var Word:String);
 Var
   Ch1:WideChar;
   i:Integer;
 begin
 if Length(Word)>0 then ch1:=Word[1] else ch1:=#0;
 if isUpAlfa(Ch1)  then
    begin
    for i:=2 to Length(Word) do
      if isUpAlfa(Word[i]) then exit;
    if ('�'<=Ch1) and (CH1<='�') then Ch1:=WideChar(Ord('�')+(Ord(Ch1)-Ord('�')));
    if ('�'=Ch1) then Ch1:='�';
    if ('A'<=Ch1) and (CH1<='Z') then Ch1:=WideChar(Ord('a')+(Ord(Ch1)-Ord('A')));
    Word[1]:=ch1;
    end;
 end;

begin
  NewArray:=TSimvols.Create(TComparer<TSimvol>.Construct(CompareSimvov));
  NewStatement:=True;
  for i:=1 to Simvols.Count-1 do
    begin
    if Simvols[i].Kind=skWord then
      begin
       Simvol:=Simvols[i];
       if NewStatement then
          MakeDown(Simvol.Value);
       NewArray.Add(Simvol);
      end;
    if Simvols[i].Value='.' then NewStatement:=True
       else  if (Simvols[i].Kind=skSpace) and (NewStatement=True) then NewStatement:=True
         else NewStatement:=False;
    end;
  Simvols.Destroy;
  Simvols:=NewArray;
end;


// �������� ��������� �����.
// ���� ���������� � �������, �� ���� �� ��������� ��� ����������
// ���� ���������� � ���������, �� ���� �� �������
//[!] �������� ��� �������� ��������
function GetFormalEnding(s:String):string;
var i, j:Integer;
begin
if (s='') then result:=''
  else
  if (s[Length(s)]='-') then result:=''
  else
  if IsGlasnyye(s[Length(s)]) then
    begin
      j:=1;
      for i := Length(s) downto 1 do
        if not(IsGlasnyye(s[i])) then
           begin
             j:=i+1;
             Break;
           end;
      result:=copy(s,j, Length(s)-j+1);
    end else
    begin
      j:=1;
      for i := Length(s) downto 1 do
        if (IsGlasnyye(s[i])) then
           begin
             j:=i;
             Break;
           end;
      result:=copy(s,j, Length(s)-j+1);
    end;

end;

{������ �� ������ ���������� ���������(��.GetFormalEnding). ����������� �������� ���������� ������.}
function DelEnding(s:String):String;
var LenEnd:Integer;
begin
if Length(s)<=3 then result:=s
  else begin
  LenEnd:=Length(GetFormalEnding(s));
  Result:=s;
  Delete(Result,Length(Result)-LenEnd+1,LenEnd);
  if Result='' then Result:=s;
  end;
end;

{������� ����������: ������ � ��� �������}
procedure PrintSimvols(Simvols:TSimvols; var Stream:TBytesStream);
var
 i:Integer;
 Bytes:TBytes;
 Last, TextLine,s:String;
 count:Integer;
begin
Stream.SetSize(0);
Last:='';
count:=0;
for i:=0 to Simvols.Count-1 do
   begin
     inc(count);
     s:=Simvols[i].Value;
     if S<>Last then
        begin
        TextLine:= String.Join(',', [Last,IntToStr(count)]);
        TextLine:=TextLine+NL;
        Bytes:=TEncoding.UTF8.GetBytes(TextLine);
        Stream.WriteData(Bytes, Length(Bytes));
        Last:=s;
        count:=0;
        end;
   end;
end;

function InvertSimvols(Simvols:TSimvols; ModeCrop:Boolean):TInvertDocument;
var Dic:TDictionary<String, TWordIndex>;
 Element:TPair<String, TWordIndex>;
 i:Integer;
 WordIndex:TWordIndex;
 Word:String;
begin
Dic:=TDictionary<String, TWordIndex>.Create;
 for i:=0 to Simvols.Count-1 do
///  if (Simvols[i].Kind=skWord) and (Simvols[i].Value<>'') then //[!] �������� �������� ������ ������� (Simvols[i].Kind=skWord) and (Simvols[i].Value='')
   begin
   if modeCrop then
      Word:=DelEnding(Simvols[i].Value)
      else Word:=Simvols[i].Value;
   if not Dic.ContainsKey(Word) then
      begin
       WordIndex.Word:=Word;
       WordIndex.pos:=TList<Integer>.Create;
       Dic.Add(Word,WordIndex);
      end;
   Dic[Word].Pos.Add(i);
   end;
SetLength(result.Words,Dic.Count);
i:=0;
for Element in Dic do
  begin
  result.Words[i]:=Element.Value;
  inc(i);
  end;
Dic.Destroy;
end;

{������� ��������������� �������� �������}
{��������� ������� �� ������ Version=1.0}
{��������� ������ ��� �����, ����� ������ ���� � �� ��������� ���� ����������}
{ModeDiff=False ������ ����� �������}
{ModeDiff=True ������ ����� �������� �� ���������� �������}
procedure PrintInvertDocumentText(InvertDocument:TInvertDocument; var Stream:TBytesStream; ModeDiff,ModeCrop:Boolean);
var
 i,j:Integer;
 Bytes:TBytes;
 Text,s:String;
 LastPos:Integer;
begin
Stream.SetSize(0);
Text:='Version=1.0'+NL;
Text:=Text+InvertDocument.FileName+NL;

for i:=0 to Length(InvertDocument.Words)-1 do
   begin
     s:=InvertDocument.Words[i].Word;
     LastPos:=0;
     for j:=0 to InvertDocument.Words[i].pos.Count-1 do
        begin
        s:=S+','+IntToStr(InvertDocument.Words[i].pos[j]-LastPos);
        if ModeDiff then LastPos:=InvertDocument.Words[i].pos[j];
        end;
     S:=S+NL;
     Text:=Text+S;
   end;
Bytes:=TEncoding.UTF8.GetBytes(Text);
Stream.WriteData(Bytes, Length(Bytes));
end;

{����� ������ � �����. ������� ������� ����� ����� ���������� ������}
procedure WriteWideString(var Stream:TBytesStream; value:WideString);
var
  Count:Integer;
  P:PWideChar;
begin
  Count:=Length(value);
  Stream.Write(Count,SizeOf(Count));
  if Count>0 then
     begin
     P:=@value[1];
     Stream.Write(P^,Count*SizeOf(WideChar));
     end;
end;

{������� ��������������� �������� ��������}
procedure PrintInvertDocumentBin(InvertDocument:TInvertDocument; var Stream:TBytesStream; ModeDiff,ModeCrop:Boolean);
var
 i,j:Integer;
 LastPos:Integer;
 Count:Integer;
 Value:Integer;
const Teg:array [0..3] of AnsiChar='idb_';   // ���������
const Version:Int32=$1;
begin
Stream.SetSize(0);
Stream.Write(Teg,SizeOf(Teg));
Stream.Write(Version,SizeOf(Version));
WriteWideString(Stream,InvertDocument.FileName);
Count:=Length(InvertDocument.Words);
Stream.Write(Count,SizeOf(Count));

for i:=0 to Count-1 do
   begin
     WriteWideString(Stream,InvertDocument.Words[i].Word);
     LastPos:=0;
     Stream.Write(InvertDocument.Words[i].pos.Count,SizeOf(Int32));
     for j:=0 to InvertDocument.Words[i].pos.Count-1 do
        begin
        Value:=InvertDocument.Words[i].pos[j]-LastPos;
        Stream.Write(Value,SizeOf(Value));
        if ModeDiff then LastPos:=InvertDocument.Words[i].pos[j];
        end;
   end;
end;

{������� ��������������� ��������}
procedure PrintInvertDocument(InvertDocument:TInvertDocument; var Stream:TBytesStream; ModeDiff,ModeCrop, IsBinary:Boolean);
begin
case IsBinary of
false: PrintInvertDocumentText(InvertDocument, Stream, ModeDiff,ModeCrop);
true: PrintInvertDocumentBin(InvertDocument, Stream, ModeDiff,ModeCrop);
end;
end;

procedure DicAddDocumen(var DocDict:TDocumentDic; DocumentID:Integer; InvertDocument:TInvertDocument);
var i:Integer;
begin
for i:=0 to Length(InvertDocument.Words)-1 do
  begin
   if not DocDict.ContainsKey(InvertDocument.Words[i].Word) then
      begin
       DocDict.Add(InvertDocument.Words[i].Word,TList<Integer>.Create);
      end;
   DocDict[InvertDocument.Words[i].Word].Add(DocumentID);
   end;
end;

//  FilePath:String;
type
TCMDParametrs=record
  DirIn:String;
  FileIn:String;
  FileOut:String;
  FileLog:String;
  DocOut:String;
  NeedHelp:Boolean;
  ModeDiff:Boolean;
  ModeCrop:Boolean;
  IsBinary:Boolean;
  end;

// �������� ��  http://forum.sources.ru/index.php?showtopic=288844
procedure ParseParametrs(var CMDParametrs:TCMDParametrs);
var
 i:Integer;
 s:String;
begin
i:=1;
 while i<= ParamCount do
   begin
   s:=ParamStr(i);
   if s='-Help' then
      begin
      CMDParametrs.NeedHelp:=True;
      Dec(i,1);
      end;
   if s='-ModeDiff' then
      begin
      CMDParametrs.ModeDiff:=True;
      Dec(i,1);
      end;
   if s='-ModeCrop' then
      begin
      CMDParametrs.ModeCrop:=True;
      Dec(i,1);
      end;
   if s='-IsBinary' then
      begin
      CMDParametrs.IsBinary:=True;
      Dec(i,1);
      end;

   if i+1> ParamCount then break;

   if s='-In' then CMDParametrs.FileIn:=ParamStr(i+1);
   if s='-DirIn' then CMDParametrs.DirIn:=ParamStr(i+1);
   if s='-DocOut' then CMDParametrs.DocOut:=ParamStr(i+1);
   if s='-Out' then CMDParametrs.FileOut:=ParamStr(i+1);
   if s='-Log' then CMDParametrs.FileLog:=ParamStr(i+1);
   Inc(i,2);
   end;

   if CMDParametrs.IsBinary=False then
     begin
     if ExtractFileExt(CMDParametrs.FileIn)='.idb' then CMDParametrs.IsBinary:=true;
     end;
if CMDParametrs.FileOut='' then
   begin
    if CMDParametrs.IsBinary then
       CMDParametrs.FileOut:=ChangeFileExt(CMDParametrs.FileIn, '.idb')
       else
       CMDParametrs.FileOut:=ChangeFileExt(CMDParametrs.FileIn, '.idt');
   end;

if (CMDParametrs.DirIn<>'') and (CMDParametrs.DocOut='')  then
  begin
  CMDParametrs.DocOut:=CMDParametrs.DirIn+TPath.GetFileName(ExcludeTrailingPathDelimiter(CMDParametrs.DirIn))+'.ddt';
  end;
end;

function TestParametrs(const CMDParametrs:TCMDParametrs):Boolean;
begin
Result:=True;
if not FileExists(CMDParametrs.FileIn) then
   begin
   WriteLn(Format('-In ���� �� ������: "%s"',[CMDParametrs.FileIn]));
   Result:=False;
   end;

 {
if not FileExists(CMDParametrs.FileLog) then
   begin
   WriteLn(Format('-Log ���� �� ������: "%s"',[CMDParametrs.FileLog]));
   Result:=False;
   end;                }
if Result=False then
   WriteLn(Format('������� �������: "%s"',[GetCurrentDir]));
end;

var
  CMDParametrs:TCMDParametrs;
  //SimvolComparer:TSimvolComparer;


procedure FreeInvertDocument(InvertDocument:TInvertDocument);
var i:Integer;
begin
for i:=0 to Length(InvertDocument.Words)-1 do
  InvertDocument.Words[i].pos.Destroy;

end;

procedure ProcessFile(FileIn, FileOut:String; DocDict:TDocumentDic; DocumentID:Integer; ModeDiff, ModeCrop, IsBinary:Boolean);
var
  Encoding:TEncoding;
  UTF8:TEncoding;
  Text:String;
  Simvols:TSimvols;
  InvertDocument:TInvertDocument;
begin
     Stream:=TBytesStream.Create;
     Stream.LoadFromFile(FileIn);

     {�������������� ����������� ��������� � �������������� � UTF8}
     {��� ������� ���������� ��� ������, ������ ������ ����������� ����� ������� ����������}
     Stream.Seek(0,soBeginning);
     Encoding:=Nil;
     UTF8:=TEncoding.UTF8.Create;
     TEncoding.GetBufferEncoding(Stream.Bytes, Encoding, UTF8);
     Text:=Encoding.GetString(Stream.Bytes);


     simvols:=LexPars(Text);
     PrepAbbr(Simvols);

///     if not IsBinary then DeleteNotWord(Simvols);
     InvertDocument:=InvertSimvols(Simvols, ModeCrop);
     InvertDocument.FileName:=ExpandFileName (FileIn);
     PrintInvertDocument(InvertDocument, Stream, ModeDiff,ModeCrop,IsBinary);
     if DocDict<>nil then
        DicAddDocumen(DocDict,DocumentID, InvertDocument);
     FreeInvertDocument(InvertDocument);
     Stream.SaveToFile(FileOut);
     Stream.Destroy;
     Simvols.Destroy;
end;


type
 PFileDesc=^TFileDesc;
 TFileDesc=record
  Path:String;
  Name:String;
  Attr:DWord;
  Size:Int64;
  end;

function MyGetFileName(s:String):String;
begin
if s[Length(s)]='\' then
 begin
 SetLength(s,Length(s)-1);
 end;
Result:=ExtractFileName(s);
end;

procedure GetFileDesc(var FileDesc:PFileDesc; Path:String; SearchRec : TSearchRec);
begin
FileDesc:=Nil;
if Path<>'' then
 begin
 GetMem(FileDesc,SizeOf(TFileDesc));
 FillChar(FileDesc^,SizeOf(TFileDesc),0);
 FileDesc^.Path:=Path;
 FileDesc^.Name:=MyGetFileName(Path);
 FileDesc^.Attr:= SearchRec.Attr;
 FileDesc^.Size:= Int64(SearchRec.FindData.nFileSizeHigh) shl 32+SearchRec.FindData.nFileSizeLow;
 end;
end;

var
  TimerIndex:Integer;

procedure GetFileDescWithShow(var FileDesc:PFileDesc; Path:String; SearchRec : TSearchRec);
var
 t:integer;
begin
GetFileDesc(FileDesc, Path, SearchRec);
t:=GetTickCount;
if t-TimerIndex>100 then
  begin
  WriteLn('procss '+Path);
 // Form1.Repaint;
  //Application.ProcessMessages;
  TimerIndex:=t;
  end;
end;

Procedure ScanDir(StartDir: String; List:TList);
Var SearchRec : TSearchRec;
 FileDesc:PFileDesc;
 Mask:string;
Begin
  TimerIndex:=GetTickCount;
  Mask:= '*.txt';
  IF StartDir[Length(StartDir)] <> '\' then StartDir := StartDir + '\';
  IF FindFirst(StartDir+Mask, faAnyFile, SearchRec) = 0 then
    Begin
     Repeat
//      Application.ProcessMessages;
      IF (SearchRec.Attr and faDirectory) <> faDirectory then
         begin
         GetFileDescWithShow(FileDesc,StartDir + SearchRec.Name,SearchRec);
         if FileDesc<> nil then
            List.Add(FileDesc);
         end else
          IF (SearchRec.Name <> '..') and (SearchRec.Name <> '.') then
            Begin
            GetFileDescWithShow(FileDesc,StartDir + SearchRec.Name + '\',SearchRec);
            if FileDesc<> nil then
               List.Add(FileDesc);
            ScanDir(StartDir + SearchRec.Name + '\',List);
            End;
     until FindNext(SearchRec) <> 0;
     FindClose(SearchRec);
   end; {IF}
end;

procedure PrintDocumentDic(DocumentDic:TDocumentDic; FileOut:String);
var Stream:TBytesStream;
 j:Integer;
 Bytes:TBytes;
 Text,s:String;
 LastPos:Integer;
 word:TPair<String,TList<Integer>>;
begin
  Stream:=TBytesStream.Create();

Stream.SetSize(0);
Text:='';
for Word in DocumentDic do
   begin
     s:=Word.Key;
     LastPos:=0;
     for j:=0 to Word.value.Count-1 do
        begin
        s:=S+','+IntToStr(Word.value[j]-LastPos);
   //     if ModeDiff then LastPos:=InvertDocument.Words[i].pos[j];
        end;
     S:=S+NL;
     Text:=Text+S;
   end;
Bytes:=TEncoding.UTF8.GetBytes(Text);
Stream.WriteData(Bytes, Length(Bytes));

  Stream.SaveToFile(FileOut);
  Stream.Destroy;
end;

procedure ProcessDir(Dir:String; DocOut:String; ModeDiff, ModeCrop,IsBinary:Boolean);
var
  FileIn, FileOut:String;
  i:Integer;
  List:TList;
  DocumentDic:TDocumentDic;
  Element:TPair<String,TList<Integer>>;
begin

 List:=TList.Create;
 ScanDir(Dir, List);
 DocumentDic:=TDocumentDic.Create;
 for i:=0 to List.Count-1 do
   begin
    FileIn:=PFileDesc(List[i]).Path;
    if IsBinary then
       FileOut:=ChangeFileExt(FileIn, '.idb')
       else
       FileOut:=ChangeFileExt(FileIn, '.idt');
    ProcessFile(FileIn, FileOut,DocumentDic, I, ModeDiff, ModeCrop, IsBinary);
    WriteLn(i,' - ',FileIn);
   end;
 PrintDocumentDic(DocumentDic, DocOut);
 for i:=0 to List.Count-1 do
   begin
     PFileDesc(List.Items[i])^.path:='';
     PFileDesc(List.Items[i])^.Name:='';
     FreeMem(PFileDesc(List.Items[i]));
   end;
 List.Destroy;
 for Element in DocumentDic do
   Element.Value.Destroy;
 DocumentDic.Destroy;
end;

procedure PrintHelp;
var ProgName:String;
begin
  ProgName:=ExtractFileName(ParamStr(0));
  WriteLn('��������� ��� ���������� ��������� �������');
  WriteLn('������� �������');
  WriteLn(ProgName+' -In <���� � �����> -Out <�������� ����>');
  WriteLn(ProgName+' -DirIn <���� � �����>');
  WriteLn(ProgName+' -Help');
  WriteLn(ProgName+' -DirIn "H:\���\" -Out ""');
  WriteLn(ProgName+' -DirIn "H:\���\" -Out "" -ModeDiff -ModeCrop');
  WriteLn(ProgName+' -DirIn "H:\�����\�����\txt Unicode\" -Out ""');
  WriteLn('-In ������ ��������� ���� � ��������� ��� �� �����');
  WriteLn('  ��������� � ������� *.idt ��� *.idb ������������ � �������� ����');
  WriteLn('-DirIn ���� ������ �������� -DirIn, �� ��������� -In � -Out ������������');
  WriteLn('  �������� ������ ������������� ���������� *.idt ��� *.idb');
  WriteLn('  � �������� ������� ������� ������ txt �����');
  WriteLn('-ModeDiff ������ �������� ������, ������� �������� ����������.');
  WriteLn('-ModeCrop ��������� ����������� ����');
  WriteLn('-ModeBinary ����� � �������� ������� (*.idb)');
  WriteLn('-Help ������� ��� �������.');
  WriteLn('������ ����� ���������� *.idt');
  WriteLn('  ��������� ������ �� 2-� �������');
  WriteLn('  ������ ������ ������ ������� version=1.0');
  WriteLn('  ������ ������ ������ ���� ����� ��� ������� ����������� ���������');
  WriteLn('  ����� � ������ ������ ����� � ����� ������� ��� ������� � �����');
  WriteLn('������ ����� ���������� *.idb');
  WriteLn('  ���������');
  WriteLn('  - 4 ������� "idb_"');
  WriteLn('  - Int32 -������ �������');
  WriteLn('  - WideString ������ ������ ���� ����� ��� ������� ����������� ���������� � �������� Unicode16');
  WriteLn('  ����');
  WriteLn('  - int32 ������ ������ ����');
  WriteLn('  - WideString �����');
  WriteLn('  - ����� ���������� ������� ���� ��������');
  WriteLn('  - ����� ������ ������� ���� ��������');
  WriteLn('�����������:');
  WriteLn('  ������������ ��� �����������, � �������� ���� �� ����� ������.');
  WriteLn('  ����������� ������.');

end;


begin
   ReportMemoryLeaksOnShutdown := True;
  { TODO -oUser -cConsole Main : Insert code here }

  ParseParametrs(CMDParametrs);
  if CMDParametrs.NeedHelp then
    begin
      PrintHelp;
    end else
    begin
    if CMDParametrs.DirIn<>'' then
       begin
       ProcessDir(CMDParametrs.DirIn,CMDParametrs.DocOut, CMDParametrs.ModeDiff, CMDParametrs.ModeCrop,CMDParametrs.IsBinary);
       end
    else if TestParametrs(CMDParametrs) then
     begin
       ProcessFile(CMDParametrs.FileIn, CMDParametrs.FileOut,nil,0,CMDParametrs.ModeDiff, CMDParametrs.ModeCrop,CMDParametrs.IsBinary);
     end else PrintHelp;
    end;
end.


﻿program Project2;

{$APPTYPE CONSOLE}

uses
  Windows, SysUtils,
  Classes,
  RegularExpressionsCore, System.Generics.Defaults, System.Generics.Collections;

var
  Stream:TBytesStream;

{PS - знак разделения парагрофов
 LS - знак разделения листов
 TB - знак табуляции
 NL - перевод строки }
const
 PS:WideChar=#$2029;
 LS:WideChar=#$2028;
 TB:WideChar=#09;
 NL:String=#13#10;

type
   {Виды лексем}
   TSimvolKind=(skNil, skOther,skStartOfFile, skEndOfFile,skSpace, skWord, skNumber, skString, skPunctuator, skComment);
   TSimvol=record
     Value:String;
     Kind:TSimvolKind;
     end;

 TSimvols=TList<TSimvol>;

 TText=record
   FFofs:Int64;
   ofs:Int64;
   FPos:Int64;
   FText:String;
   FCurrentSimvol:TSimvol;
   FNextSimvol:TSimvol;
   end;
var
 f:TText;

{Специальные виды лексем}
const
 SimvolNil:TSimvol=(Value:''; Kind:skNil);
 SimvolSOT:TSimvol=(Value:''; Kind:skStartOfFile); // SOT - Start of text
 SimvolEOT:TSimvol=(Value:''; Kind:skEndOfFile);   // EOT - End of text

//
function ReadNextSimvol(var Text:TText):TSimvol; forward;
function ReadWord(var Text:TText):TSimvol; forward;

{Сбрасываем состояние на начало}
procedure Reset(var f :TText);
begin
f.ofs:=1;
f.FFofs:=1;
F.FPos:=1;
f.FCurrentSimvol:=ReadNextSimvol(F);
f.FNextSimvol:=ReadNextSimvol(F);
end;

procedure Assign(var f :TText; Text:String);
begin
F.FText:=Text;
end;

{Функция провекри конца теста}
function EOT(const f:TText):Boolean;
begin
result:= f.ofs>Length(f.FText);
end;

{Функция провекри начала теста}
function SOT(const f:TText):Boolean;
begin
result:= f.ofs=1;
end;

{Возврщает текущий символ}
function CurrentChar(const f:TText):WideChar;
begin
Result:=#0;
if (0<f.ofs) and (f.ofs<=Length(f.FText)) then
   result:= f.FText[f.ofs];
end;

{Возврщает следующий символ без смещения указателя}
function NextChar(const f:TText):WideChar;
var Ofs1:Int64;
begin
Result:=#0;
Ofs1:=f.ofs+1;
if (0<Ofs1) and (Ofs1<=Length(f.FText)) then
   result:= f.FText[Ofs1];
end;

{Вычитывает следующий символ при этом смещает указателя на следующий символ}
function ReadChar(var f:TText):WideChar;
begin
Result:=#0;
if (0<f.ofs) and (f.ofs<=Length(f.FText)) then
   begin
   result:= f.FText[f.ofs];
   inc(f.ofs);
   end;
end;

function FCurrentChar(const f:TText):WideChar;
begin
Result:=#0;
if (0<f.FFofs) and (f.FFofs<=Length(f.FText)) then
   result:= f.FText[f.FFofs];
end;

function FNextChar(const f:TText):WideChar;
var Ofs1:Int64;
begin
Result:=#0;
Ofs1:=f.FFofs+1;
if (0<Ofs1) and (Ofs1<=Length(f.FText)) then
   result:= f.FText[Ofs1];
end;

function FReadChar(var f:TText):WideChar;
begin
Result:=#0;
if (0<f.FFofs) and (f.FFofs<=Length(f.FText)) then
   begin
   result:= f.FText[f.FFofs];
   inc(f.FFofs);
   f.ofs:=f.FFofs;
   end;
end;


function FEOT(const f:TText):Boolean;
begin
result:= f.FFofs>Length(f.FText);
end;

function FSOT(const f:TText):Boolean;
begin
result:= f.FFofs=1;
end;

function IsAlfa(const ch:WideChar):Boolean;
begin
Result:=(('А'<=Ch) and (CH<='Я') ) or (Ch='Ё') or
        (('а'<=Ch) and (CH<='я') ) or (Ch='ё') or
        (('A'<=Ch) and (CH<='Z') ) or
        (('a'<=Ch) and (CH<='z') );
end;

function isUpAlfa(const ch:WideChar):Boolean;
begin
Result:=(('А'<=Ch) and (CH<='Я') ) or (Ch='Ё') or
        (('A'<=Ch) and (CH<='Z') );
end;


function IsWordAlfa(const ch:WideChar):Boolean;
begin
Result:=IsAlfa(ch);
result:=Result or ('_'=Ch) or ('-'=Ch) or ('¬'=Ch);
end;

function IsDigital(const ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, ['0'..'9']);
end;

function IsNewLineChar(const ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, [#10, #13]);
Result:= Result or (Ch=LS) or (Ch=PS);
end;

function IsSpace(const ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, [#9, #10, #11, #12, #13, #32]);
Result:= Result or IsNewLineChar(ch);
end;

function IsPunctuation(const ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, ['!', '"', '#', '$', '%', '&', '''', '(', ')', '*',
                       '+', ',', '-', '.', '/', ':', ';', '<', '=', '>',
                       '?', '@', '[', '\', ']', '^', '`', '{', '|', '}',
                       '~']);

end;

function IsOther(ch:WideChar):Boolean;
begin
Result:=Not (IsDigital(ch) or IsWordAlfa(ch) or IsSpace(ch) or IsPunctuation(ch));
end;

function IsUri_gen_delims(ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, [':', '/', '?', '#', '[', ']', '@']);
end;

function IsUri_sub_delims(ch:WideChar):Boolean;
begin
Result:=CharInSet(Ch, ['!', '$', '''', '(', ')', '*', '+', ',', ';', '=']);
end;

function IsUri_reserved(ch:WideChar):Boolean;
begin
Result:=IsUri_gen_delims(ch) or IsUri_sub_delims(ch);
end;

function IsUri_unreserved(ch:WideChar):Boolean;
begin
Result:=IsAlfa(Ch) or IsDigital(Ch);
Result:=Result or CharInSet(Ch, ['-', '.', '_', '~']);
end;

function IsUriChar(ch:WideChar):Boolean;
begin
Result:=IsUri_unreserved(Ch) or
        CharInSet(Ch,['%', ';', '?', ':', '@', '&', '=', '+', '$', ',', '/']);

end;

function ReadNumber(var Text:TText): TSimvol;
var
  s:WideString;
begin
 S:='';
 while IsDigital(FCurrentChar(Text)) or (FCurrentChar(Text)='.') do
   begin
     S:=S+FReadChar(Text);
   end;
 Result.value:=s;
 Result.Kind:=skNumber;
end;

function ReadOther(var Text:TText): TSimvol;
var
  s:WideString;
begin
 S:='';
 while IsOther(FCurrentChar(Text)) and (not FEOT(Text)) do
   begin
     S:=S+FReadChar(Text);
   end;
 Result.value:=s;
 Result.Kind:=skOther;
end;

function ReadPunctuator(var Text:TText): TSimvol;
var
  s:String;
  ch1,ch2:WideChar;
begin
 S:='';
 if IsPunctuation(FCurrentChar(Text)) then
     begin
     ch1:=FReadChar(Text);
     S:=S+Ch1;
     Ch2:=FCurrentChar(Text);
     if IsPunctuation(Ch2) then
         begin

         if ((ch1='<') and (ch2='>')) or
            ((ch1='<') and (ch2='=')) or
            ((ch1='>') and (ch2='=')) or
            ((ch1=':') and (ch2='=')) then
             begin
             Ch2:=FReadChar(Text);
             S:=s+Ch2;
             end;
         end;
     end;

 Result.value:=s;
 Result.Kind:=skPunctuator;
end;

function ReadNewLine(var Text:TText):TSimvol;
var
  s:String;
begin
 S:='';
 if IsNewLineChar(FCurrentChar(Text)) then
   begin
   if FCurrentChar(Text)=#13 then
    begin
    S:=S+FReadChar(Text);
    if FCurrentChar(Text)=#10 then
       S:=S+FReadChar(Text);
    end else S:=S+FReadChar(Text);
   end;
 Result.value:=s;
 Result.Kind:=skSpace;
end;

function ReadSpace(var Text:TText): TSimvol;
var
  s:String;
begin
 S:='';
 if IsNewLineChar(FCurrentChar(Text)) then
    begin
    Result:=ReadNewLine(Text);
    exit;
    end
    else
    begin
    S:=FReadChar(Text);
    while IsSpace(FCurrentChar(Text))and (not IsNewLineChar(FCurrentChar(Text)) ) do
     begin
     S:=S+FReadChar(Text);
     end;
   end;
 Result.value:=s;
 Result.Kind:=skSpace;
end;

function ReadEot(var Text:TText): TSimvol;
begin
 Result:=SimvolEOT;
end;

{Проверяем оканчивается слово переносом}
{Используется состояние конечного автомата из Text}
function IsHyphen(Word:String; var Text:TText):boolean;
begin
Result:=False;
 if (Word[Length(Word)]='-') or (Word[Length(Word)]='¬') then
      if IsNewLineChar(FCurrentChar(Text)) then
        Result:=True;
end;

{Вычитываем перенос и вторую половину слова}
{Входным первая половина с символом переноса, выходная слово целиком без переноса}
procedure ReadHyphenWord(var s:String; var Text:TText);
var SecondWord:TSimvol;
begin
 Delete(s, Length(s),1);
 ReadNewLine(Text);
 SecondWord:=ReadWord(Text);
 S:=S+SecondWord.Value;
end;

function ReadWord(var Text:TText):TSimvol;
var
  s:String;
begin
 S:='';
 if IsWordAlfa(FCurrentChar(Text)) then
   begin
   if (FCurrentChar(Text)='-') then
       S:=S+FReadChar(Text)
     else while (IsWordAlfa(FCurrentChar(Text))) or (IsDigital(FCurrentChar(Text))) do
     begin
       S:=S+FReadChar(Text);
     end;
     {Перенос}
     if IsHyphen(s, Text) then
          ReadHyphenWord(s,Text);
   end;
 Result.value:=s;
 Result.Kind:=skWord;
end;

function ReadNextSimvol(var Text:TText):TSimvol;
 function ReadStartFromPunctuation(var Text:TText):TSimvol;
 begin
   Result:=ReadPunctuator(Text);
 end;
var
  Ch:WideChar;
begin
  Ch:=FCurrentChar(Text);
  if IsWordAlfa(Ch) then Result:=ReadWord(Text)
     else if IsSpace(Ch) then Result:=ReadSpace(Text)
        else if IsDigital(Ch) then Result:=ReadNumber(Text)
           else if IsPunctuation(Ch) then Result:=ReadStartFromPunctuation(Text)
                else Result:=ReadOther(Text);
  if FEot(Text) then Result:=ReadEot(Text);

end;

function ReadSimvol(var Text:TText): TSimvol;
begin
 Result:=Text.FCurrentSimvol;
 Text.FCurrentSimvol:=Text.FNextSimvol;
 Text.FNextSimvol:=ReadNextSimvol(Text);
 Inc(Text.FPos);
end;


function CompareSimvov(const Left, Right: TSimvol): Integer;
begin
  if Left.Value<Right.Value then Result:=-1
     else if Left.Value=Right.Value then Result:=0
       else Result:=+1
end;

{Преобразует сырой текст в список символов}
function LexPars(Text:String):TSimvols;
var
  t:TText;
begin
Result:=TList<TSimvol>.Create(TComparer<TSimvol>.Construct(CompareSimvov)); // Создаем спско символов с функцией сравнения для сортировки
Result.Add(SimvolSOT);

 Assign(T,Text);
 Reset(T);
 repeat
 Result.Add(ReadSimvol(T));
 until EOT(T);
 Result.Add(ReadSimvol(T));

end;

{Вычленении аббревиатур с точкой. Аббревиатуры состоящие из нескольких символов заменяются одним символом}
{Такая процедура необходима для удаления из потока символов знака точки который не относитсяк концу предложения. а применен для сокращения слов}
{Метод основан на словаре и должен быть пополнен}
procedure PrepAbbr(var simvols:TSimvols);
var
  i:Integer;
  ForDelete:TList<integer>;
  Abrr:Boolean;
  Simvol_1:TSimvol;
  s:String;
  procedure CheckDelete(i:Integer);
  var
    Simvol__1, Simvol__2:TSimvol;
  begin
   if i+1<=Simvols.Count-1 then   Simvol__1:=Simvols[i+1] else Simvol__1:=SimvolNil;
   if i+2<=Simvols.Count-1 then Simvol__2:=Simvols[i+2] else Simvol__2:=SimvolNil;
   if (Simvol__1.Kind=skSpace) and
       (Length(Simvol__2.Value)>=1) and
       not isUpAlfa(Simvol__2.Value[1]) then
       ForDelete.Add(i);
   if  (Length(Simvol__1.Value)>=1) and
       not isUpAlfa(Simvol__1.Value[1]) then
       ForDelete.Add(i);

  end;
begin
ForDelete:=TList<integer>.Create;
for i:=1 to Simvols.Count-1 do
  begin
  if (Simvols[i].Value='.') then
     begin
     Simvol_1:=Simvols[i-1];
     s:=Simvol_1.Value;
     Abrr:=False;
     if (S='т') then Abrr:=True;   //т.е. - то есть
     if (S='е') then Abrr:=True;   //т.е. - то есть
     if (S='д') then Abrr:=True;   //т.д. - так далее
     if (S='п') then Abrr:=True;   //т.п. - тому подобное
     if (S='П') then Abrr:=True;   //П.С. - после письма
     if (S='г') then Abrr:=True;   //г. - год
     if (S='гг') then Abrr:=True;  //гг. - года
     if (S='с') then Abrr:=True;   //с. - секунды
     if (S='С') then Abrr:=True;   //П.С. - после письма
     if (S='В') then Abrr:=True;   //В. - вольты
     if (S='кг') then Abrr:=True;  //кг. - килограммы
     if (S='вт') then Abrr:=True;  //вт. - ваты
     if (S='Вт') then Abrr:=True;  //Вт. - ваты
     if (S='А') then Abrr:=True;   //А. - амперы
     if (S='мин') then Abrr:=True; //мин. - минуты
     if (S='сек') then Abrr:=True; //сек. - секунды
     if (S='Рис') then Abrr:=True; //Рис. - рисунок
     if (S='рис') then Abrr:=True; //рис. - рисунок

     if Abrr then
        begin
        Simvol_1.Value:=Simvol_1.Value+'.';
        Simvols[i-1]:=Simvol_1;
        CheckDelete(i);
        end;
     end;
  end;
for i:=ForDelete.Count-1 downto 0 do
   Simvols.Delete(ForDelete[i]);

ForDelete.Clear;

for i:=1 to Simvols.Count-1 do
  begin
  if (Simvols[i-1].Value='т.') and (Simvols[i].Value='е.') then
     begin
     Simvol_1:=Simvols[i-1];
     Simvol_1.Value:='т.е.';
     Simvols[i-1]:=Simvol_1;
     ForDelete.Add(i);
     end;
  if (Simvols[i-1].Value='т.') and (Simvols[i].Value='д.') then
     begin
     Simvol_1:=Simvols[i-1];
     Simvol_1.Value:='т.д.';
     Simvols[i-1]:=Simvol_1;
     ForDelete.Add(i);
     end;
  if (Simvols[i-1].Value='т.') and (Simvols[i].Value='п.') then
     begin
     Simvol_1:=Simvols[i-1];
     Simvol_1.Value:='т.п.';
     Simvols[i-1]:=Simvol_1;
     ForDelete.Add(i);
     end;
  if (Simvols[i-1].Value='П.') and (Simvols[i].Value='С.') then
     begin
     Simvol_1:=Simvols[i-1];
     Simvol_1.Value:='П.С.';
     Simvols[i-1]:=Simvol_1;
     ForDelete.Add(i);
     end;
  end;

for i:=ForDelete.Count-1 downto 0 do
   Simvols.Delete(ForDelete[i]);
end;

{Удаляем математический мусор вида P1,I75, а так же пунктуацию
Походу дела переводим слова написанные заглавными в прописные искл}
procedure DeleteNotWord(var Simvols:TSimvols);
var
 NewArray:TSimvols;
 i:Integer;
 Simvol:TSimvol;
 NewStatement:Boolean;
 procedure MakeDown(var Word:String);
 Var
   Ch1:WideChar;
   i:Integer;
 begin
 if Length(Word)>0 then ch1:=Word[1] else ch1:=#0;
 if isUpAlfa(Ch1)  then
    begin
    for i:=2 to Length(Word) do
      if isUpAlfa(Word[i]) then exit;
    if ('А'<=Ch1) and (CH1<='Я') then Ch1:=WideChar(Ord('а')+(Ord(Ch1)-Ord('А')));
    if ('Ё'=Ch1) then Ch1:='ё';
    if ('A'<=Ch1) and (CH1<='Z') then Ch1:=WideChar(Ord('a')+(Ord(Ch1)-Ord('A')));
    Word[1]:=ch1;
    end;
 end;

begin
  NewArray:=TSimvols.Create(TComparer<TSimvol>.Construct(CompareSimvov));
  NewStatement:=True;
  for i:=1 to Simvols.Count-1 do
    begin
    if Simvols[i].Kind=skWord then
      begin
       Simvol:=Simvols[i];
       if NewStatement then
          MakeDown(Simvol.Value);
       NewArray.Add(Simvol);
      end;
    if Simvols[i].Value='.' then NewStatement:=True
       else  if (Simvols[i].Kind=skSpace) and (NewStatement=True) then NewStatement:=True
         else NewStatement:=False;
    end;
  Simvols.Destroy;
  Simvols:=NewArray;
end;


{Выводим статистику: символ и его частоту}
procedure PrintSimvols(Simvols:TSimvols; var Stream:TBytesStream);
var
 i:Integer;
 Bytes:TBytes;
 Last, TextLine,s:String;
 count:Integer;
begin
Stream.SetSize(0);
Last:='';
count:=0;
for i:=0 to Simvols.Count-1 do
   begin
     inc(count);
     s:=Simvols[i].Value;
     if S<>Last then
        begin
        TextLine:= String.Join(',', [Last,IntToStr(count)]);
        TextLine:=TextLine+NL;
        Bytes:=TEncoding.UTF8.GetBytes(TextLine);
        Stream.WriteData(Bytes, Length(Bytes));
        Last:=s;
        count:=0;
        end;
   end;
end;

{Выводим разделенные слова}
procedure PrintNatashaSimvols(Simvols:TSimvols; var Stream:TBytesStream);
var
 i:Integer;
 Bytes:TBytes;
 Text,s:String;
begin
Stream.SetSize(0);
for i:=0 to Simvols.Count-1 do
   begin
     s:=Simvols[i].Value;
     Text:= S+'|';
     Bytes:=TEncoding.UTF8.GetBytes(Text);
     Stream.WriteData(Bytes, Length(Bytes));
   end;
end;

//  FilePath:String;
type
TCMDParametrs=record
  DirIn:String;
  FileIn:String;
  FileOut:String;
  FileLog:String;
  NeedHelp:Boolean;
  ModeNatasha:Boolean;
  end;

// Заменить на  http://forum.sources.ru/index.php?showtopic=288844
procedure ParseParametrs(var CMDParametrs:TCMDParametrs);
var
 i:Integer;
 s:String;
begin
i:=1;
 while i+1<= ParamCount do
   begin
   s:=ParamStr(i);
   if s='-In' then CMDParametrs.FileIn:=ParamStr(i+1);
   if s='-DirIn' then CMDParametrs.DirIn:=ParamStr(i+1);
   if s='-Out' then CMDParametrs.FileOut:=ParamStr(i+1);
   if s='-Log' then CMDParametrs.FileLog:=ParamStr(i+1);
   if s='-Help' then
      begin
      CMDParametrs.NeedHelp:=True;
      Dec(i,1);
      end;
   if s='-ModeNatasha' then
      begin
      CMDParametrs.ModeNatasha:=True;
      Dec(i,1);
      end;
   Inc(i,2);
   end;

if CMDParametrs.FileOut='' then
   begin
    CMDParametrs.FileOut:=ChangeFileExt(CMDParametrs.FileIn, '.tb');
   end;
end;

function TestParametrs(const CMDParametrs:TCMDParametrs):Boolean;
begin
Result:=True;
if not FileExists(CMDParametrs.FileIn) then
   begin
   WriteLn(Format('-In Файл не найден: "%s"',[CMDParametrs.FileIn]));
   Result:=False;
   end;

 {
if not FileExists(CMDParametrs.FileLog) then
   begin
   WriteLn(Format('-Log Файл не найден: "%s"',[CMDParametrs.FileLog]));
   Result:=False;
   end;                }
if Result=False then
   WriteLn(Format('Текущий каталог: "%s"',[GetCurrentDir]));
end;

var
  CMDParametrs:TCMDParametrs;
  //SimvolComparer:TSimvolComparer;


procedure ProcessFile(FileIn, FileOut:String; ModeNatasha:Boolean);
var
  Encoding:TEncoding;
  UTF8:TEncoding;
  Text:String;
  Simvols:TSimvols;
begin
     Stream:=TBytesStream.Create;
     Stream.LoadFromFile(FileIn);

     {Автоматическое определение кодировки и преобразование к UTF8}
     {Код обходит внутренний баг дельфи, вместо прямой подстановки нужно создать переменные}
     Stream.Seek(0,soBeginning);
     Encoding:=Nil;
     UTF8:=TEncoding.UTF8.Create;
     TEncoding.GetBufferEncoding(Stream.Bytes, Encoding, UTF8);
     Text:=Encoding.GetString(Stream.Bytes);


     simvols:=LexPars(Text);
     PrepAbbr(Simvols);

     if ModeNatasha then
       begin
       PrintNatashaSimvols(Simvols, Stream);
       end else
       begin
       DeleteNotWord(Simvols);
       //SimvolComparer:=TSimvolComparer.Create;
       Simvols.Sort;
       PrintSimvols(Simvols, Stream);
       end;
     Stream.SaveToFile(FileOut);
     Stream.Destroy;
end;


type
 PFileDesc=^TFileDesc;
 TFileDesc=record
  Path:String;
  Name:String;
  Attr:DWord;
  Size:Int64;
  end;

function MyGetFileName(s:String):String;
begin
if s[Length(s)]='\' then
 begin
 SetLength(s,Length(s)-1);
 end;
Result:=ExtractFileName(s);
end;

procedure GetFileDesc(var FileDesc:PFileDesc; Path:String; SearchRec : TSearchRec);
begin
FileDesc:=Nil;
if Path<>'' then
 begin
 GetMem(FileDesc,SizeOf(TFileDesc));
 FillChar(FileDesc^,SizeOf(TFileDesc),0);
 FileDesc^.Path:=Path;
 FileDesc^.Name:=MyGetFileName(Path);
 FileDesc^.Attr:= SearchRec.Attr;
 FileDesc^.Size:= Int64(SearchRec.FindData.nFileSizeHigh) shl 32+SearchRec.FindData.nFileSizeLow;
 end;
end;

var
  TimerIndex:Integer;

procedure GetFileDescWithShow(var FileDesc:PFileDesc; Path:String; SearchRec : TSearchRec);
var
 t:integer;
begin
GetFileDesc(FileDesc, Path, SearchRec);
t:=GetTickCount;
if t-TimerIndex>100 then
  begin
  WriteLn('procss '+Path);
 // Form1.Repaint;
  //Application.ProcessMessages;
  TimerIndex:=t;
  end;
end;

Procedure ScanDir(StartDir: String; List:TList);
Var SearchRec : TSearchRec;
 FileDesc:PFileDesc;
 Mask:string;
Begin
  TimerIndex:=GetTickCount;
  Mask:= '*.txt';
  IF StartDir[Length(StartDir)] <> '\' then StartDir := StartDir + '\';
  IF FindFirst(StartDir+Mask, faAnyFile, SearchRec) = 0 then
    Begin
     Repeat
//      Application.ProcessMessages;
      IF (SearchRec.Attr and faDirectory) <> faDirectory then
         begin
         GetFileDescWithShow(FileDesc,StartDir + SearchRec.Name,SearchRec);
         if FileDesc<> nil then
            List.Add(FileDesc);
         end else
          IF (SearchRec.Name <> '..') and (SearchRec.Name <> '.') then
            Begin
            GetFileDescWithShow(FileDesc,StartDir + SearchRec.Name + '\',SearchRec);
            if FileDesc<> nil then
               List.Add(FileDesc);
            ScanDir(StartDir + SearchRec.Name + '\',List);
            End;
     until FindNext(SearchRec) <> 0;
     FindClose(SearchRec);
   end; {IF}
end;

procedure ProcessDir(Dir:String; ModeNatasha:Boolean);
var
  FileIn, FileOut:String;
  i:Integer;
  List:TList;
begin
 List:=TList.Create;
 ScanDir(Dir, List);
 for i:=0 to List.Count-1 do
   begin
    FileIn:=PFileDesc(List[i]).Path;
    FileOut:=ChangeFileExt(FileIn, '.tb');
    ProcessFile(FileIn, FileOut, ModeNatasha);
    WriteLn(i,' - ',FileIn);
   end;
 List.Destroy;
end;

procedure PrintHelp;
var ProgName:String;
begin
  ProgName:=ExtractFileName(ParamStr(0));
  WriteLn('Программа для нарезки текста на слова и подсчета их повтора');
  WriteLn('Примеры запуска');
  WriteLn(ProgName+' -In <путь к файлу> -Out <Выходной файл>');
  WriteLn(ProgName+' -DirIn <путь к папке>');
  WriteLn(ProgName+' -ModeNatasha');
  WriteLn(ProgName+' -Help');
  WriteLn(ProgName+' -DirIn "H:\пас\" -Out ""');
  WriteLn(ProgName+' -DirIn "H:\книги\общий\txt Unicode\" -Out ""');
  WriteLn('-In читает текстовый файл и разделяет его на слова');
  WriteLn('  Результат в формате *.tb записывается в выходной файл');
  WriteLn('-DirIn Если указан параметр -DirIn, то результат -In и -Out игнорируются');
  WriteLn('  Выходным файлам присваивается расширение *.tb');
  WriteLn('  В качестве входных берутся только txt файлы');
  WriteLn('-ModeNatasha меняет выходной формат.');
  WriteLn('-Help выводит эту справку.');
  WriteLn('Формат файла результата *.tb');
  WriteLn('  В каждой строке слово и через запятую его количество повторов');
  WriteLn('Формат Наташи');
  WriteLn('  Слова и лексемы разделяются через символ "|" ');
  WriteLn('Особенности:');
  WriteLn('  Распознается ряд аббревиатур, и переносы слов на новую строку.');
  WriteLn('  Фильтруется муссор.');

end;


begin
  { TODO -oUser -cConsole Main : Insert code here }

  ParseParametrs(CMDParametrs);
  if CMDParametrs.NeedHelp then
    begin
      PrintHelp;
    end else
    begin
    if CMDParametrs.DirIn<>'' then
       begin
       ProcessDir(CMDParametrs.DirIn,CMDParametrs.ModeNatasha);
       end;
    if TestParametrs(CMDParametrs) then
     begin
       ProcessFile(CMDParametrs.FileIn, CMDParametrs.FileOut,CMDParametrs.ModeNatasha);
     end else PrintHelp;
    end;
end.
